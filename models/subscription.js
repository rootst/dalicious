'use strict';
var _ = require('lodash');
var mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');


var subscriptionSchema = new mongoose.Schema({
    owner: { type: mongoose.Schema.Types.ObjectId, ref: "User", required: true },
    createdDate: { type: Date, default: Date.now },
    status: { type: String, required: true },
    team: {type: String},

    peopleCounts: { type: Number, required: true },
    //budget: { type: Number, required: true },
    //serviceDate: { type: Date, required: true },
    serviceAddress: { type: String, required: true },
    //cuisines: { type: String, required: true },
    description: { type: String }
});


subscriptionSchema.plugin(mongoosePaginate);

if (_.indexOf(mongoose.modelNames(), 'Subscription') === -1) {
    module.exports = mongoose.model('Subscription', subscriptionSchema);
} else {
    module.exports = mongoose.model('Subscription');
}
