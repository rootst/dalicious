'use strict';
var _ = require('lodash');
var mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');


var reviewSchema = new mongoose.Schema({
    owner: { type: mongoose.Schema.Types.ObjectId, ref: "User", required: true },
    auction: { type: mongoose.Schema.Types.ObjectId, ref: "Auction", required: true },
    createdDate: { type: Date, default: Date.now },
    title: { type: String },
    content: { type: String },
    pictures: [{
        type: String
    }]
});


reviewSchema.plugin(mongoosePaginate);

if (_.indexOf(mongoose.modelNames(), 'Review') === -1) {
    module.exports = mongoose.model('Review', reviewSchema);
} else {
    module.exports = mongoose.model('Review');
}
