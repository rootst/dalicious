'use strict';
var _ = require('lodash');
var mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');
var i18n = require('mongoose-i18n');
var bcrypt = require('bcrypt');

//var config = require('./../configs/config');

var userSchema = new mongoose.Schema({
    role: [{ type: String, required: true }],
    email: { type: String, unique: true },
    password: { type: String },
    joinedDate: { type: Date, default: Date.now },
    lastLoggedInDate: { type: Date, default: Date.now },
    displayName: { type: String, required: true },
    address: [{ type: String }],
    avatar: { type: String, default: '' },
    gender: { type: String },
    dateOfBirth: { type: Date },
    introduce: {type: String, default: ''},

    phone: {
        number: { type: String },
        verified: { type: Boolean, default: false },
        temporary: { type: Number }
    },

    supplier: {
        authenticated: { type: Boolean },
        cuisines: [{ type: String }],
        menus: [{ type: String }]
    },

    spaceSupplier: {

    },
    consumer: {

    },
    administrator: {

    },

    facebook: {
        id: { type: String },
        accessToken: { type: String }
    },
    kakao: {
        id: { type: String }
    }

});

userSchema.pre('save', function(next) {
    var User = this;
    if (!_.isUndefined(User.password) && (User.isModified('password') || User.isNew)) {
        bcrypt.genSalt(10, (errorResult, salt) => {
            if (errorResult) {
                console.error(errorResult);
                return next(errorResult);
            }
            bcrypt.hash(User.password, salt, (errorResult, hash) => {
                if (errorResult) {
                    console.error(errorResult);
                    return next(errorResult);
                }
                User.password = hash;
                next();
            });
        });
    } else {
        console.log('password hash pass');
        return next();
    }
});

userSchema.methods.comparePassword = function(password, callback) {
    bcrypt.compare(password, this.password, (errorResult, isMatch) => {
        if (errorResult) {
            errorResult._status = 500;
            return callback(errorResult, null);
        }
        callback(null, isMatch);
    });
};


userSchema.plugin(mongoosePaginate);
//userSchema.plugin(i18n, {languages: config.locales, defaultLanguage: config.defaultLocale});

if (_.indexOf(mongoose.modelNames(), 'User') === -1) {
    module.exports = mongoose.model('User', userSchema);
} else {
    module.exports = mongoose.model('User');
}
