'use strict';
var _ = require('lodash');
var mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');


var bidSchema = new mongoose.Schema({
    owner: { type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true },
    auction: { type: mongoose.Schema.Types.ObjectId, ref: 'Auction', required: true },
    price: { type: Number, required: true },
    proposal: { type: String },
    createdDate: { type: Date, default: Date.now }
});

bidSchema.pre('save', function(next) {
    if (this.isNew) {
        var config = require('../configs/config');
        var Auction = require(config.root + '/models/auction');

        Auction.find({_id: this.auction}).then((result) => {
            if (!result) {
                var errorResult = {};
                errorResult._status = 500;
                return next(errorResult);
            }

            _.forEach(result.targetSuppliers, (targetSupplier) => {
                if (targetSupplier.user === this.owner) {
                    targetSupplier.bidable = false;
                }
            });
            next();
        });
    } else {
        next();
    }
});

bidSchema.plugin(mongoosePaginate);

if (_.indexOf(mongoose.modelNames(), 'Bid') === -1) {
    module.exports = mongoose.model('Bid', bidSchema);
} else {
    module.exports = mongoose.model('Bid');
}