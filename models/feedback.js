'use strict';
var _ = require('lodash');
var mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');


var feedbackSchema = new mongoose.Schema({
  name: { type: String, required: true },
  phone: { type: String, required: true },
  content: { type: String, required: true },
  createdDate: { type: Date, default: Date.now }
});

feedbackSchema.pre('save', function(next) {
  next();
});

feedbackSchema.plugin(mongoosePaginate);

if (_.indexOf(mongoose.modelNames(), 'Feedback') === -1) {
  module.exports = mongoose.model('Feedback', feedbackSchema);
} else {
  module.exports = mongoose.model('Feedback');
}