'use strict';
var _ = require('lodash');
var mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');
var schedule = require('node-schedule');

var config = require('../configs/config');
var Aws = require(config.root + '/utilities/aws');


var auctionSchema = new mongoose.Schema({
    owner: { type: mongoose.Schema.Types.ObjectId, ref: "User", required: true },
    targetSuppliers: [{
        bidable: {type: Boolean},
        user: {type: mongoose.Schema.Types.ObjectId, ref: "User"}
    }],
    createdDate: { type: Date, default: Date.now },
    status: { type: String, required: true },
    bids: [{ type: mongoose.Schema.Types.ObjectId, ref: "Bid" }], // NOT USED

    winner: { type: mongoose.Schema.Types.ObjectId, ref: "Bid" },
    chosenBids: [{ type: mongoose.Schema.Types.ObjectId, ref: "Bid" }],

    peopleCounts: { type: Number, required: true },
    budget: { type: Number, required: true },
    serviceDate: { type: Date, required: true },
    serviceStartTime: { type: Date, required: true },
    serviceAddress: { type: String, required: true },
    cuisines: { type: String, required: true },
    description: { type: String }
});

auctionSchema.pre('save', function(next) {
    if (this.isNew) {
        var Bid = require(config.root + '/models/bid');

        var triggerDate = new Date();
        triggerDate.setDate(triggerDate.getDate() + 1);
        var job = schedule.scheduleJob(triggerDate, function(thisAuctionSchema) {
            thisAuctionSchema.status = config.auctionStatus.completed;

            var phoneNumber = thisAuctionSchema.owner.phone.number;
            var messageContent = '';
            messageContent = 'Dalicious입니다. 귀하의 경매가 완료되었습니다.';
            Aws.publishSMS(
                messageContent,
                'string',
                phoneNumber,
                (sendError, sent) => {
                    if (sendError) {
                        console.error(sendError);
                    }

                    // find TOP3
                    // sort, limit 3
                    // remove sort
                    Bid.find({auction: thisAuctionSchema, price: {$lte: thisAuctionSchema.budget}}).sort({price: -1}).limit(3).then((result) => {
                        _.forEach(result, (bid) => {
                            thisAuctionSchema.chosenBids.push(bid);
                        });

                        if (_.isEmpty(result)) {
                            thisAuctionSchema.status = config.auctionStatus.failure;
                        }

                        thisAuctionSchema.save((saveErrorResult, saveResult) => {
                            if (saveErrorResult) {
                                console.error(saveErrorResult);
                                return;
                            }
                        });
                    });
                }
            );
        }.bind(null, this));
        
        this.sendSmsOrPushMessage((error, result) => {

        });
        this.createNotification((error, result) => {
            next();
        });

    } else {
        next();
    }
});


auctionSchema.methods.findSuppliers = function(callback) {
    var User = require(config.root + '/models/user');
    
    var regex = new RegExp("(([가-힣]+(시|도)|[서울]|[인천]|[대구]|[광주]|[부산]|[울산])( |)[가-힣]+(시|군|구))");
    var extractAddresses = regex.exec(this.serviceAddress);

    if (!extractAddresses) {
        return callback([]);
    }

    var usefulAddresses = [extractAddresses[2]];

    // 광역, 특별을 제거해야한다.
    if (_.includes(usefulAddresses[0], '광역시') || _.includes(usefulAddresses[0], '특별시')) {
        usefulAddresses[0] = usefulAddresses[0].substring(0, usefulAddresses[0].length - 3);
        usefulAddresses[0] = usefulAddresses[0] + '시';
    }
    // 시 없는거 시 붙여야 한다.
    if (!_.includes(usefulAddresses[0], '시')) {
        usefulAddresses[0] = _.split(extractAddresses.input, ' ')[0] + '시';
    }

    // 주소 맨앞에 시가 없으면 callback[]을 보내야 한다.


    User.find({role: {$in: config.suppliers}, address: {$in: usefulAddresses}}, (errorResult, result) => {
        if (errorResult) {
            return callback([]);
        }
        if (!result) {
            return callback([]);
        }

        return callback(result);
    });
};

auctionSchema.methods.sendSmsOrPushMessage = function(callback) {
    this.findSuppliers((foundSuppliers) => {
        _.forEach(foundSuppliers, (foundSupplier) => {
            // filter by cuisines

            var phoneNumber = foundSupplier.phone.number;
            var messageContent = '';
            messageContent = 'Dalicious입니다. 귀하가 입찰할 수 있는 경매가 있습니다.';
            Aws.publishSMS(
                messageContent,
                'string',
                phoneNumber,
                (sendError, sent) => {
                    if (sendError) {
                        return callback(sendError, null);
                    }
                }
            );
        });
        callback(null, null);
    });
};

auctionSchema.methods.createNotification = function(callback) {
    this.findSuppliers((foundSuppliers) => {
        var targetSuppliers = [];
        _.forEach(foundSuppliers, (foundSupplier) => {
            if (foundSupplier.supplier.authenticated) {
                var field = {};
                field.bidable = true;
                field.user = foundSupplier;
                targetSuppliers.push(field);
            }
        });

        this.targetSuppliers = targetSuppliers;
        callback(null, null);
    });
};


auctionSchema.plugin(mongoosePaginate);

if (_.indexOf(mongoose.modelNames(), 'Auction') === -1) {
    module.exports = mongoose.model('Auction', auctionSchema);
} else {
    module.exports = mongoose.model('Auction');
}
