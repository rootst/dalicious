function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}
function csrfSafeMethod(method) {
    return ( /^(HEAD|OPTIONS|TRACE)$/.test(method)) ;
}
function sameOrigin(url) {
    var host = document.location.host;
    var protocol = document.location.protocol;
    var sr_origin = '//' + host;
    var origin = protocol + sr_origin;
    return (url == origin || url.slice(0, origin.length + 1) == origin + '/') || (url == sr_origin || url.slice(0, sr_origin.length + 1) == sr_origin + '/') || !(/^(\/\/|http:|https:).*/.test(url));
}
function timeSince(strDate) {
    var date = new Date(strDate);
    var seconds = Math.floor((new Date() - date) / 1000);
    var interval = Math.floor(seconds / 31536000);

    if (interval >= 1) {
        return interval + "년 전";
    }
    interval = Math.floor(seconds / 2592000);
    if (interval >= 1) {
        return interval + "달 전";
    }
    interval = Math.floor(seconds / 86400);
    if (interval >= 1) {
        return interval + "일 전";
    }
    interval = Math.floor(seconds / 3600);
    if (interval >= 1) {
        return interval + "시간 전";
    }
    interval = Math.floor(seconds / 60);
    if (interval >= 1) {
        return interval + "분 전";
    }
    return Math.floor(seconds) + "초 전";

    /*
     0~60: 초전
     61~3600: 분전
     */
}

function moneyToKorean(num) {
    var hanA = new Array("","일","이","삼","사","오","육","칠","팔","구","십");
    var danA = new Array("","십","백","천","","십","백","천","","십","백","천","","십","백","천");
    var result = "";
    for(i=0; i<num.length; i++) {
        str = "";
        han = hanA[num.charAt(num.length-(i+1))];
        if(han != "")
            str += han+danA[i];
        if(i == 4) str += "만";
        if(i == 8) str += "억";
        if(i == 12) str += "조";
        result = str + result;
    }
    if(num != 0)
        result = result + "원";
    return result ;
}

function numberWithCommas(x) {
    return x.toString().replace(',', '').replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function numberFilter(value){
    return numberWithCommas(value.replace(/[^0-9]/gi, ""));
}

function makePictureInfo(item) {
    var ret = "";
    var strPictures = JSON.stringify(item.pictures);
    
    
    if(item.pictures.length === 1) {
        ret =
            `
                <div class="review__item--picture row no-gutters" data-pictures='${strPictures}'>
                  <div class="col-md-12 col-xs-12">
                    <div class="pic-inner"><img src="${item.pictures[0]}" data-index="0" class="review-image"></div>
                  </div>
                </div>
            `;
        return ret;
    }

    if(item.pictures.length === 2) {
        ret =
            `
                <div class="review__item--picture row no-gutters" data-pictures='${strPictures}'>
                  <div class="col-md-6 col-xs-6">
                    <div class="pic-inner"><img src="${item.pictures[0]}" data-index="0" class="review-image"></div>
                  </div>
                  <div class="col-md-6 col-xs-6">
                    <div class="pic-inner"><img src="${item.pictures[1]}" data-index="1" class="review-image"></div>
                  </div>
                </div>
            `;
        return ret;
    }

    if(item.pictures.length === 3) {
        ret =
            `
                <div class="review__item--picture row no-gutters" data-pictures='${strPictures}'>
                  <div class="col-md-12 col-xs-12">
                    <div class="pic-inner"><img src="${item.pictures[0]}" data-index="0" class="review-image"></div>
                  </div>
                  <div class="col-md-6 col-xs-6">
                    <div class="pic-inner"><img src="${item.pictures[1]}" data-index="1" class="review-image"></div>
                  </div>
                  <div class="col-md-6 col-xs-6">
                    <div class="pic-inner"><img src="${item.pictures[2]}" data-index="2" class="review-image"></div>
                  </div>
                </div>
            `;
        return ret;
    }

    if(item.pictures.length >= 4) {
        ret =
            `
                <div class="review__item--picture row no-gutters" data-pictures='${strPictures}'>
                  <div class="col-md-12 col-xs-12">
                    <div class="pic-inner"><img src="${item.pictures[0]}" data-index="0" class="review-image"></div>
                  </div>
                  <div class="col-md-4 col-xs-4">
                    <div class="pic-inner"><img src="${item.pictures[1]}" data-index="1" class="review-image"></div>
                  </div>
                  <div class="col-md-4 col-xs-4">
                    <div class="pic-inner"><img src="${item.pictures[2]}" data-index="2" class="review-image"></div>
                  </div>
                  <div class="col-md-4 col-xs-4">
                    <div class="pic-inner"><img src="${item.pictures[3]}" data-index="3" class="review-image"></div>
                  </div>
                </div>
            `;
        return ret;
    }

    return "";
    
}

function makeOwnerInfo(item) {
    var ret =
        `
        <div class="review__item--owner">
          <div class="name">${item.owner.displayName} 님</div>
          <div class="date">FEB. 15</div>
        </div>
        `;

    return ret;
}

function makeContentInfo(item) {
    var ret =
        `
        <div class="review__item--title"><span class="title">${item.title}</span></div>
        <div class="review__item--content">
          <p>${item.content}</p>
        </div>
        `;
    return ret;
}

function makeAvatar(item) {
    var ret = "";

    if(item.owner.avatar === "") {
        ret = "/images/profile.png";
    } else {
        ret = item.owner.avatar;
    }
    return ret;
}

function loadReviewList() {
    var jwtToken = localStorage.getItem('jwt');
    $.ajax({
        type: 'GET',
        url: '/api/reviews',
        headers: {
            'Authorization': 'JWT ' + jwtToken
        },
        success: function (data) {
            var avatar = "",
                ownerInfo = "",
                pictures = "",
                content = "";

            var $wrapObject = "";

            if (data.data.total) {
                $.each(data.data.items, function (index, item) {
                    avatar = makeAvatar(item);
                    ownerInfo = makeOwnerInfo(item);
                    pictures = makePictureInfo(item);
                    content = makeContentInfo(item);

                    if(index % 2) {
                        $wrapObject = $(".review__item > .right");
                    } else {
                        $wrapObject = $(".review__item > .left");
                    }

                    $wrapObject.append(
                        `
                        <div class="inner row no-gutters">
                          <div class="col-md-2"><img src="${avatar}" class="profile-image"></div>
                          <div class="col-md-10">
                            ${ownerInfo}
                          </div>
                          <div class="col-md-12">
                            ${pictures}
                          </div>
                          <div class="col-md-12">
                            ${content}
                          </div>
                        </div>
                        `
                    );

                });
            }
        }
    });
}

function loadBidedList() {
    var jwtToken = localStorage.getItem('jwt');
    var params = {
        owner: globalUserId
    };
    $.ajax({
        type: 'GET',
        url: '/api/bids',
        headers: {
            'Authorization': 'JWT ' + jwtToken
        },
        data: params,
        success: function(data) {
            if(data.data.total) {
                $.each(data.data.items, function (index, item) {
                    var serviceDate = new Date(item.auction.serviceDate);
                    $('table > tbody').append(`
                    <tr>
                        <td class='col-2'>${timeSince(item.auction.createdDate)}</td>
                        <td class='col-8'>
                            ${item.auction.description}
                        </td>
                        <td class='col-1'>${item.price}</td>
                        <td class='col-1'><input type="button" value="보기" data-index="${item.auction._id}" class="btn btn-primary bid-detail-btn"></td>
                    </tr>
                    <tr class="detail ${item.auction._id}">
                        <td class='col-2'></td>
                        <td class="col-8">
                            <ul>
                                <li><span class='text-bold'>날짜</span>: ${serviceDate.getFullYear() + "-" + serviceDate.getMonth() + 1 + "-" + serviceDate.getDate()}</li>
                                <li><span class='text-bold'>인원수</span>: ${item.auction.peopleCounts}</li>
                                <li><span class='text-bold'>주소</span>: ${item.auction.serviceAddress}</li>
                            </ul>
                        </td>
                        <td class='col-1'></td>
                        <td class='col-1'></td>
                    </tr>
                `)
                });
            } else {
                $('table.request-list > tbody').append(`
                    <tr>
                        <td class='col-2'></td>
                        <td class='col-8'>입찰가능 목록이 없습니다.</td>
                        <td class='col-1'></td>
                        <td class='col-1'></td>
                    </tr>
                `)
            }
        }
    });
}
function loadBidList() {
    var jwtToken = localStorage.getItem('jwt');
    var params = {
        targetSuppliers: globalUserId,
        status: 'PROCESSING'
    };
    $.ajax({
        type: 'GET',
        url: '/api/auctions',
        headers: {
            'Authorization': 'JWT ' + jwtToken
        },
        data: params,
        success: function(data) {
            if(data.data.total) {
                $.each(data.data.items, function (index, item) {
                    // 0 index = me
                    //if(item.targetSuppliers[0].bidable) {
                        var serviceDate = new Date(item.serviceDate);
                        $('table.request-list > tbody').append(`
                            <tr>
                                <td class='col-2'>${timeSince(item.createdDate)}</td>
                                <td class='col-8'>
                                    ${item.description}
                                </td>
                                <td class='col-1'><input type="button" value="입찰" data-index="${item._id}" class="btn btn-primary open-bid-modal"></td>
                                <td class='col-1'><input type="button" value="보기" data-index="${item._id}" class="btn btn-primary bid-detail-btn"></td>
                            </tr>
                            <tr class="detail ${item._id}">
                                <td class='col-2'></td>
                                <td class="col-8">
                                    <ul>
                                        <li><span class='text-bold'>날짜</span>: ${serviceDate.getFullYear() + "-" + serviceDate.getMonth() + 1 + "-" + serviceDate.getDate()}</li>
                                        <li><span class='text-bold'>인원수</span>: ${item.peopleCounts}</li>
                                        <li><span class='text-bold'>주소</span>: ${item.serviceAddress}</li>
                                    </ul>
                                </td>
                                <td class="col-1"></td>
                                <td class="col-1"></td>
                            </tr>
                        `)
                    //}
                });
            } else {
                $('table.request-list > tbody').append(`
                    <tr>
                        <td class='col-2'></td>
                        <td class='col-8'>입찰가능 목록이 없습니다.</td>
                        <td class='col-1'></td>
                        <td class='col-1'></td>
                    </tr>
                `)
            }
        }
    });
}
function loadSubscriptionList() {
    var jwtToken = localStorage.getItem('jwt');
    $.ajax({
        type: 'GET',
        url: '/api/subscriptions',
        headers: {
            'Authorization': 'JWT ' + jwtToken
        },
        success: function(data) {
            if(data.data.total) {
                $.each(data.data.items, function (index, item) {
                    // 0 index = me
                    //if(item.targetSuppliers[0].bidable) {
                        var serviceDate = new Date(item.serviceDate);
                        var phoneNumber = "";
                        if(typeof item.owner.phone.number !== "undefined") {
                            phoneNumber = item.owner.phone.number;
                        }
                        $('table.request-list > tbody').append(`
                            <tr>
                                <td class='col-2'>${timeSince(item.createdDate)}</td>
                                <td class='col-2'>
                                    ${item.owner.displayName} / ${item.team}
                                </td>
                                <td class='col-2'>
                                    ${phoneNumber}
                                </td>
                                <td class='col-6'>${item.description}</td>
                            </tr>
                           
                        `)
                    //}
                });
            } else {
                $('table.request-list > tbody').append(`
                    <tr>
                        <td class='col-2'></td>
                        <td class='col-2'>입찰가능 목록이 없습니다.</td>
                        <td class='col-2'></td>
                        <td class='col-6'></td>
                    </tr>
                `)
            }
        }
    });
}
function loadAuctionListToConsumer() {
    var jwtToken = localStorage.getItem('jwt');
    var params = {
        owner: globalUserId
    };
    $.ajax({
        type: 'GET',
        url: '/api/auctions',
        headers: {
            'Authorization': 'JWT ' + jwtToken
        },
        data: params,
        success: function(data) {
            if(data.data.total) {
                $.each(data.data.items, function (index, item) {
                    var statusText = "";
                    var statusSummary = "진행중";
                    if(item.status === "PROCESSING") {
                        statusText = "알맞은 공급자들에게 신청이 전달되어 입찰이 진행중입니다.";
                    } else if(item.status === "COMPLETED") {
                        statusText = "공급자 세명의 선정이 완료됐습니다, 공급자 선정 후 결제를 진행해주세요.";
                        statusSummary = "<input type='button' class='btn btn-primary select-sup-btn' data-index='" + item._id + "' value='진행'>";
                    } else if(item.status === "SUCCESS") {
                        statusText = "결제가 완료되었습니다, 곧 담당자로부터 연락이 갑니다. 조금만 기다려주세요.";
                        statusSummary = "결제완료"
                    } else if(item.status === "SERVICE-END") {
                        statusText = "서비스는 만족하셨나요? 받은 서비스에대해 리뷰를 작성해보세요!";
                        statusSummary = "<input type='button' class='btn btn-primary open-review-modal-btn' data-index='" + item._id + "' value='리뷰작성'>"
                    } else if(item.status === "REVIEW-COMPLETED") {
                        statusText = "소중한 리뷰작성 감사합니다. 다음에 또 만나요!";
                        statusSummary = "최종완료"
                    } else if(item.status === "FAILURE") {
                        statusText = "해당 경매에 입찰한 멤버스가 없습니다, 금액을 조정해보세요!";
                        statusSummary = "경매실패"
                    }
                    $('table > tbody').append(`
                    <tr data-index="${item._id}" class="${item.status.toLowerCase()}">
                        <td class='col-2'>${timeSince(item.createdDate)}</td>
                        <td class='col-1'>${numberWithCommas(item.budget)}</td>
                        <td class='col-7 status'>
                                ${statusText}
                        </td>
                        <td class='col-2'>${statusSummary}</td>
                    </tr>
                `)
                });
            } else {
                $('table > tbody').append(`
                    <tr>
                        <td class='col-2'></td>
                        <td class='col-1'></td>
                        <td class='col-7'>신청 내역이 없습니다.</td>
                        <td class='col-2'></td>
                    </tr>
                `)
            }
        }
    })
}

var Dalicious = {
    host: 'http://localhost:3000',
    init: function() {
        var jwtToken = localStorage.getItem('jwt');
        if(jwtToken !== null) {
            $.ajaxSetup({
                beforeSend: function (xhr, settings) {
                    if (!csrfSafeMethod(settings.type) && sameOrigin(settings.url)) {
                        xhr.setRequestHeader("Authorization", 'JWT ' + jwtToken);
                    }
                },
            });
        }
    }
};

$(function() {


});

$(document).ready(function() {
    Dalicious.init();

    $('.eat-box.mobile-text').slick({
        dots: true,
    });

    $(".show-mobile-gnb").click(function(e){
        e.preventDefault();
        $(".mobile-gnb-cover").show();
        $(".mobile-gnb").addClass("animated slideInRight");
        $(".mobile-gnb").show();
    });

    $(".hide-mobile-gnb").click(function(e){
        e.preventDefault();
        $(".mobile-gnb").hide();
        $(".mobile-gnb-cover").hide();
    });

    $(".mobile-gnb-cover").click(function(){
        $(".mobile-gnb").hide();
        $(".mobile-gnb-cover").hide();
    });

    $(".user-members-apply").click(function(){
        var params = {
            role: $('select[name=members-apply-type]').val()
        };

        $.ajax({
            type: 'PATCH',
            url: '/api/users/' + $(this).attr('data-index'),
            data: params,
            statusCode: {
                401: function(data) {

                },
                403: function(data) {
                    alert('핸드폰 번호 인증 후 멤버스 신청이 가능합니다.');
                },
                404: function(data) {
                }
            },
            success: function(data) {
                alert('신청 완료, 달리셔스에서 검토후 인증이 완료되면 활동이 가능합니다.');
                window.location.href = "/mypage?menu=members_profile";
            }
        })
    });

    $(".members-join-btn").click(function() {
        $('.open-join-modal').click();
        $('.join-tab--members > a').click();

        /*$('.join-toggle').click(function(e){
            e.preventDefault();
            if(!$(this).hasClass('join-tab--in')) {
                var dataType = $(this).attr('data-type');
                if(dataType === "members") {
                    $('.join-tab--user').removeClass('join-tab--in');
                    $('.members-type-field').show();
                } else {
                    $('.join-tab--members').removeClass('join-tab--in');
                    $('.members-type-field').hide();
                }
                $('#join-form').attr('data-type', dataType);
                $(this).parent().addClass('join-tab--in');
            }
        });*/

    });

    $(".swap-service-a").click(function(e) {
        e.preventDefault();
        var dest = $(this).attr("data-dest");
        var src = $(this).attr("data-src");
        var effect = "flipInY";

        var $srcDom = $(".request-wrapper." + src);
        var $destDom = $(".request-wrapper." + dest);

        $(".tab-btn." + dest + "-btn").removeClass("off");
        $("." + dest + "-img").attr('src', '/images/' + dest + '-on.png');
        $(".tab-btn." + src+ "-btn").addClass("off");
        $("." + src + "-img").attr('src', '/images/' + src + '-off.png');

        $("a[data-src='" + src + "']").addClass("on");
        $("a[data-src='" + dest + "']").removeClass("on");


        if(dest === "ondemand") {
            $(".sub-step-image").hide();
            $(".ondemand-step-image").show();
        } else {
            $(".ondemand-step-image").hide();
            $(".sub-step-image").show();
        }
        
        
        $srcDom.hide();
        $srcDom.removeClass(effect);
        $destDom.show();
        $destDom.addClass(effect);

    });

    //-- setting jwt header
    $(".center").slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 3,
        arrows: false,
        autoplay: true,
        autoplaySpeed: 2000,
        variableWidth: true
    });

    $(".carousel").slick({
        dots: true,
        infinite: true,
        speed: 300,
        slidesToShow: 1,
        adaptiveHeight: true
    });

    $('.logout-a').click(function(){
        localStorage.removeItem('jwt');
    });

    $(document).on('click', '.delete-zone-a', function(e){
        e.preventDefault();
        var value = $(this).attr('data-value');
        $(this).parent().remove();
        $('.available-zone').append(`<option value='${value}'>${value}</option>`);
    });

    $('.available-zone').change(function(){
        var value = $(this).val();

        // 오른쪽에 Zone 박스 추가
        $('.zone-list').append(`
            <div class='myzone ${value}'>
                <span class="zone-title">${value}</span> 
                <a href='#' class='delete-zone-a' data-value='${value}'><span class='fa fa-times'></span></a>
            </div>`);
        // 리스트에서 해당 주소 삭제
        $('option[value=' + value + ']').remove();
        $(this).prop('selectedIndex', 0);

    });

    $(document).on('click', '.delete-cuisines-a', function(e){
        e.preventDefault();
        var value = $(this).attr('data-value');
        var text = $(this).attr('data-text');
        $(this).parent().remove();
        $('.available-cuisines').append(`<option value='${value}'>${text}</option>`);
    });

    $('.available-cuisines').change(function(){
        var value = $(this).val();
        var text = $('option[value=' + value + ']').text();

        // 오른쪽에 Zone 박스 추가
        $('.cuisines-list').append(`
            <div class='mycuisines ${value}'>
                <span class="cuisines-title" data-value="${value}">${text}</span> 
                <a href='#' class='delete-cuisines-a' data-value='${value}' data-text='${text}'><span class='fa fa-times'></span></a>
            </div>`);
        // 리스트에서 해당 주소 삭제
        $('option[value=' + value + ']').remove();
        $(this).prop('selectedIndex', 0);

    });

    $(document).on('click', '.bid-detail-btn', function(){
        var index = $(this).attr('data-index');
        $('tr.' + index).show();
    });

    $(document).on('click', '.select-sup-btn', function() {
        var index = $(this).attr('data-index');
        location.href = '/bids/?auction=' + index;
    });
    
    $('.choose-sup-btn').click(function(){
        var auctionId = $(this).attr('data-auction');
        var winner = $(this).attr('data-index');
        $.ajax({
            type: 'PATCH',
            url: '/api/auctions/' + auctionId,
            data: {
                winner: winner
            },
            success: function(data) {
                alert('해당 서비스의 선택이 완료됐습니다, 담당자로부터 곧 연락이 가니 기다려주세요.');
                location.href = '/mypage?menu=request';
            }
        })   
    });
    
    $("input[name='avatar']").change(function(){
        var val = $(this).val();
        switch (val.substring(val.lastIndexOf('.') + 1).toLowerCase()) {
            case 'gif':
            case 'jpg':
            case 'png':
            case 'jpeg':
                var formData = new FormData();
                formData.append("avatar", $("input[name='avatar']")[0].files[0]);
                $.ajax({
                    url: "/avatar",
                    data: formData,
                    type: 'POST',
                    cache: false,
                    processData: false,
                    contentType: false,
                    error: function (data) {

                    },
                    success: function (data) {
                        $('.profile-image').attr('src', data.data);
                    },
                });
                break;
            default:
                $(this).val('');
                alert("gif,jpg,png 파일만 업로드 가능해요");
                break;
        }
    });

    $('.member-modify-btn').click(function(e){
        var address = [];
        var cuisines = [];
        var menus = [];

        $('div.myzone').each(function(index) {
            var zone = $(this).children('.zone-title').text();
            address.push(zone);
        });

        $('div.mycuisines').each(function(index) {
            var zone = $(this).children('.cuisines-title').attr('data-value');
            cuisines.push(zone);
        });

        menus.push($("input[name=food1]").val());
        menus.push($("input[name=food2]").val());
        menus.push($("input[name=food3]").val());


        var params = {
            'address': address,
            'cuisines': cuisines,
            'introduce': $('input[name=introduce]').val(),
            'menus': menus
        };

        $.ajax({
            type: 'PATCH',
            url: '/api/users/' + globalUserId,
            contentType: "application/json",
            dataType: 'json',
            data: JSON.stringify(params),
            statusCode: {
                401: function(data) {
                    //alert('아이디 또는 비밀번호가 다릅니다.');
                },
                404: function(data) {
                    //alert('존재하지 않는 계정입니다.');
                }
            },
            success: function(data) {
                alert('저장 완료!');
            }
        })
    });

    $('.user-modify-submit').click(function(e){
        var params = {
            displayName: $('input[name=user-name]').val(),
            gender: $('select[name=gender]').val(),
            'phone.number': $('input[name=user-phone]').val()
        };

        $.ajax({
            type: 'PATCH',
            url: '/api/users/' + $(this).attr('data-index'),
            data: params,
            statusCode: {
                401: function(data) {
                    //alert('아이디 또는 비밀번호가 다릅니다.');
                },
                404: function(data) {
                    //alert('존재하지 않는 계정입니다.');
                }
            },
            success: function(data) {
                alert('저장 완료');
            }
        })
    });

    $('#bid-form').submit(function(e){
        e.preventDefault();
        var auctionPK = $('.bid-modal').attr('data-index');
        var bidPrice = $('.bid-price').val();
        var bidProposal = $('.bid-proposal').val();

        $.ajax({
            type: 'POST',
            url: '/api/bids',
            data: {
                auction: auctionPK,
                price: bidPrice,
                proposal: bidProposal
            },
            success: function(data) {
                alert('입찰 성공');
                location.href = '/mypage?menu=bid';
            }
        });
    });

    $(document).on('click', '.open-bid-modal', function(e) {
        e.preventDefault();
        $('.bid-price').val('');
        $('.bid-proposal').val('');
        $('.bid-modal').attr('data-index', $(this).attr('data-index'));
        $('.bid-modal').modal();
    });

    $('.open-login-modal').click(function(e) {
        e.preventDefault();
        $(".join-section").hide();
        $(".join-form").hide();
        $(".login-section").show();
        $(".login-form").addClass("animated slideInRight").show();
    });

    $(".close-login-form").click(function(e) {
        $(".login-section").hide();
        $(".login-form").hide();
    });

    $(".close-request-modal").click(function(e){
        e.preventDefault();
        var type = $(this).attr("data-type");
        $(".request-" + type + "-service").modal('hide');
    });

    $(".white-cover").click(function(){
        $(".login-section").hide();
        $(".login-form").hide();
        $(".join-section").hide();
        $(".join-form").hide();
    });

    //-- login submit
    $('#login-form').submit(function(e) {
        e.preventDefault();
        var email = $('.login-email').val();
        var password = $('.login-password').val();
        var $errorLabel;

        if(!validateEmail(email)) {
            $errorLabel = $('.login-email').next();
            $errorLabel.text('* 올바른 이메일 형식이 아닙니다.');
            $errorLabel.show();
            return;
        }
        var params = {
            email: email,
            password: password,
        };

        $.ajax({
            type: 'POST',
            url: '/login',
            data: params,
            statusCode: {
                401: function(data) {
                    alert('아이디 또는 비밀번호가 다릅니다.');
                },
                404: function(data) {
                    alert('존재하지 않는 계정입니다.');
                }
            },
            success: function(data) {
                localStorage.setItem('jwt', data.data);
                location.href = '/';
            }
        });
    });

    $('.open-join-modal').click(function(e) {
        e.preventDefault();
        $(".login-section").hide();
        $(".login-form").hide();
        $(".join-section").show();
        $(".join-form").addClass("animated slideInRight").show();
    });

    $(".close-join-form").click(function(e) {

        $(".join-section").hide();
        $(".join-form").hide();
    });

    //-- join submit
    $('#join-form').submit(function(e) {
        e.preventDefault();

        var email = $('.join-email').val();
        var password = $('.join-password').val();
        var password2 = $('.join-password2').val();
        var name = $('.join-name').val();
        var $errorLabel;
        var dataType = $(this).attr('data-type');

        $(this).find('.error').hide();

        if(!validateEmail(email)) {
            $errorLabel = $('.join-email').next();
            $errorLabel.text('* 올바른 이메일 형식이 아닙니다.');
            $errorLabel.show();
            return;
        }

        if(password !== password2) {
            $errorLabel = $('.join-password2').next();
            $errorLabel.text('* 비밀번호가 일치하지 않습니다.');
            $errorLabel.show();
            return;
        }


        var params = {
            email: email,
            password: password,
            displayName: name
        };

        if(dataType === 'CONSUMER') {
            params.role = 'CONSUMER';
        } else {
            params.role = $("select[name=members-type]").val();
        }

        $.ajax({
            type: 'POST',
            url: '/api/users',
            data: params,
            success: function(data) {
                localStorage.setItem('jwt', data.data);
                if(dataType === 'CONSUMER') {
                    location.href = '/';
                } else {
                    alert('가입이 완료됐습니다. 프로필을 추가하시면 관리자 확인 후 멤버스 인증이 완료됩니다.');
                    location.href = '/mypage?menu=members_profile';
                }
            }
        });

    });

    //-- join type toggle
    $('.join-toggle').click(function(e){
        e.preventDefault();
        if(!$(this).hasClass('join-tab--in')) {
            var dataType = $(this).attr('data-type');
            if(dataType === "members") {
                $('.join-tab--user').removeClass('join-tab--in');
                $('.members-type-field').show();
            } else {
                $('.join-tab--members').removeClass('join-tab--in');
                $('.members-type-field').hide();
            }
            $('#join-form').attr('data-type', dataType);
            $(this).parent().addClass('join-tab--in');
        }
    });
    function subscription() {
        var date,
            team,
            people,
            zone,
            cuisines,
            description;

        //date = $('#service-date-modal').val();
        team = $('#service-job2').val();
        people = $('#service-people2').val();
        zone = $('#service-zone2').val();
        //cuisines = $('#service-cuisines-modal').val();
        description = $('#service-describe2').val();

        if(people.length * zone.length === 0) {
            alert('*표시된 필드를 모두 채운 뒤 신청해주세요');
            return;
        }

        var params = {
            team: team,
            peopleCounts: people,
            serviceAddress: zone,
            //cuisines: cuisines,
            description: description
        };

        $.ajax({
            type: 'POST',
            url: '/api/subscriptions',
            data: params,
            statusCode: {
                500: function(data) {
                    alert('오류가 발생했습니다.');
                },
                403: function(data) {
                    alert('노쇼 예방을 위해, 핸드폰 인증을 거친 뒤 서비스 신청이 가능합니다.\n[ MYPAGE ] > [ 개인정보수정 ]');
                    if (globalUserId) {
                        location.href = '/mypage';
                    } else {
                        $('.login-modal').modal();
                    }
                },
                400: function(data) {

                }
            },
            success: function(data) {
                alert('신청이 완료되었습니다. 곧 담당자로부터 연락이 갑니다');
                location.href = '/';
            }
        });
    }
    function ondemand() {
        var date,
            budget,
            people,
            zone,
            cuisines,
            description,
            startTime,
            endTime;


        var serviceType = $('select[name=service-type]').val();
        date = $('#service-date').val();
        budget = $('#service-budget').val();
        people = $('#service-people').val();
        zone = $('#service-zone').val();
        cuisines = $('#service-cuisines').val();
        //cuisines = $("input[name=service-cuisines-hidden]").val();
        description = $('#service-describe').val();
        startTime = $('#service-starttime').val();
        //endTime = $('#service-endtime').val();




        if(date.length * budget.length * people.length * zone.length * startTime.length === 0) {
            alert('*표시된 필드를 모두 채운 뒤 신청해주세요');
            return;
        }

        var params = {
            serviceDate: date,
            budget: budget,
            peopleCounts: people,
            serviceAddress: zone,
            cuisines: cuisines,
            description: description,
            serviceStartTime: startTime
        };

        if(serviceType === "festival") {
            params.auctionType = "FESTIVAL";
        }

        $.ajax({
            type: 'POST',
            url: '/api/auctions',
            data: params,
            statusCode: {
                403: function(data) {
                    alert('노쇼 예방을 위해, 핸드폰 인증을 거친 뒤 서비스 신청이 가능합니다.\n[ MYPAGE ] > [ 개인정보수정 ]');
                    if (globalUserId) {
                        location.href = '/mypage';
                    } else {
                        $('.login-modal').modal();
                    }
                },
                500: function(data) {
                    alert(data.responseJSON.data);
                }
            },
            success: function(data) {
                alert('신청이 완료되었습니다.');
                location.href = '/mypage?menu=request';
            }
        });
    }

    $('.request-service-btn').click(function() {
        if($(this).attr('data-type') === "subscription") {
            subscription();
        } else {
            ondemand();
        }
    });

    $('.request-service-modal-btn').click(function() {
        var date,
            budget,
            people,
            zone,
            cuisines,
            description;

        date = $('#service-date-modal2').val();
        budget = $('#service-budget-modal2').val();
        people = $('#service-people-modal2').val();
        zone = $('#service-zone-modal2').val();
        cuisines = $('#service-cuisines-modal2').val();
        description = $('#service-describe-modal2').val();

        if(date.length * budget.length * people.length * zone.length === 0) {
            alert('*표시된 필드를 모두 채운 뒤 신청해주세요');
            return;
        }

        var params = {
            serviceDate: date,
            budget: budget,
            peopleCounts: people,
            serviceAddress: zone,
            cuisines: cuisines,
            description: description
        };

        $.ajax({
            type: 'POST',
            url: '/api/auctions',
            data: params,
            statusCode: {
                500: function(data) {
                    alert(data.data);
                },
                403: function(data) {
                    alert('노쇼 예방을 위해, 핸드폰 인증을 거친 뒤 서비스 신청이 가능합니다.\n[ MYPAGE ] > [ 개인정보수정 ]');
                    if (globalUserId) {
                        location.href = '/mypage';
                    } else {
                        $('.login-modal').modal();
                    }
                },
                400: function(data) {

                }
            },
            success: function(data) {
                alert('신청이 완료되었습니다.');
                location.href = '/mypage?menu=request';
            }
        });
    });

    $('.request-service-subscribe-btn').click(function() {
        var date,
            team,
            people,
            zone,
            cuisines,
            description;

        //date = $('#service-date-modal').val();
        team = $('#service-job-modal').val();
        people = $('#service-people-modal').val();
        zone = $('#service-zone-modal').val();
        cuisines = $('#service-cuisines-modal').val();
        description = $('#service-describe-modal').val();

        if(people.length * zone.length === 0) {
            alert('*표시된 필드를 모두 채운 뒤 신청해주세요');
            return;
        }

        var params = {
            //serviceDate: date,
            //budget: budget,
            team: team,
            peopleCounts: people,
            serviceAddress: zone,
            cuisines: cuisines,
            description: description
        };

        $.ajax({
            type: 'POST',
            url: '/api/subscriptions',
            data: params,
            statusCode: {
                500: function(data) {
                    alert(data.data);
                },
                403: function(data) {
                    alert('노쇼 예방을 위해, 핸드폰 인증을 거친 뒤 서비스 신청이 가능합니다.\n[ MYPAGE ] > [ 개인정보수정 ]');
                    if (globalUserId) {
                        location.href = '/mypage';
                    } else {
                        $('.login-modal').modal();
                    }
                },
                400: function(data) {

                }
            },
            success: function(data) {
                alert('신청이 완료되었습니다.');
                location.href = '/mypage?menu=request';
            }
        });
    });

    //--date picker
    var dateToday = new Date();
    $('#service-date').datepicker({
        minDate: dateToday,
        monthNames: ['1 월','2 월','3 월','4 월','5 월','6 월','7 월','8 월','9 월','10 월','11 월','12 월'], // 개월 텍스트 설정
        monthNamesShort: ['1 월','2 월','3 월','4 월','5 월','6 월','7 월','8 월','9 월','10 월','11 월','12 월'], // 개월 텍스트 설정
        dayNames: ['일요일', '월요일','화요일','수요일','목요일','금요일','토요일'], // 요일 텍스트 설정
        dayNamesMin: ['일','월','화','수','목','금','토'], // 요일 텍스트 축약 설정    dayNamesMin: ['월','화','수','목','금','토','일'], // 요일 최소 축약 텍스트 설정
    });

    $('#service-date-modal2').datepicker({
        minDate: dateToday,
        monthNames: ['1 월','2 월','3 월','4 월','5 월','6 월','7 월','8 월','9 월','10 월','11 월','12 월'], // 개월 텍스트 설정
        monthNamesShort: ['1 월','2 월','3 월','4 월','5 월','6 월','7 월','8 월','9 월','10 월','11 월','12 월'], // 개월 텍스트 설정
        dayNames: ['일요일', '월요일','화요일','수요일','목요일','금요일','토요일'], // 요일 텍스트 설정
        dayNamesMin: ['일','월','화','수','목','금','토'], // 요일 텍스트 축약 설정    dayNamesMin: ['월','화','수','목','금','토','일'], // 요일 최소 축약 텍스트 설정
    });

    //-- header fix
    var $header = $('.header');
    $(window).scroll(function(){
        //if($serviceDom.length) {
            if ($(window).scrollTop() + 70 >= 100) {
                if (!$header.hasClass('header--fixed')) {
                    $header.addClass('header--fixed animated slideInDown');
                }
            } else {
                if ($header.hasClass('header--fixed')) {
                    $header.removeClass('header--fixed animated slideInDown');
                }
            }
        //}
    });

    $('.inner.select').click(function(e) {
        e.preventDefault();
        var type = $(this).attr('data-type');
        if(type === "ondemand") {
            $('.request-ondemand-service').modal();
        } else {
            $('.request-subscribe-service').modal();
        }
    });

    $('.open-ondemand-modal').click(function(e) {
       // e.preventDefault();
        $('.request-ondemand-service').modal();
    });

    $(".phone-auth-request-btn").click(function(){
        $.ajax({
            type: "POST",
            url: '/api/users/' + globalUserId + "/phone_verified",
            data: {
                phoneNumber: $("input[name='user-phone']").val(),
            },
            success: function(res) {
                var countDown = 60 * 3,
                    display = $('span.time');
                startTimer(countDown, display);
                $(".auth1").hide();
                $(".auth2").show();
            }
        });

    });

    $(".phone-auth-btn").click(function(){
        $.ajax({
            type: "POST",
            url: '/api/users/' + globalUserId + "/phone_verified",
            data: {
                authNumber: $("input[name='user-auth-number']").val(),
            },
            success: function(res) {
                alert("인증완료, 핸드폰 번호 저장이 완료됐습니다");
                window.location.href = "/";
            },
            error: function(res) {
                alert("인증번호가 다릅니다.");
            }
        });
    });

    $(".add-file").click(function(){
        $(".review-write__file--wrapper").append(`
        <div class="review-write__file--item row no-gutters">
          <div class="col-md-11">
            <input type="file" name="image[]">
        </div>
         <div class="col-md-1">
            <input type="button", value="지우기">
            </div>
        </div>`);
    });

    $("input[name=images]").change(function(){
        var val = $(this).val();
        switch (val.substring(val.lastIndexOf('.') + 1).toLowerCase()) {
            case 'gif':
            case 'jpg':
            case 'png':
            case 'jpeg':
                var formData = new FormData();
                formData.append("image", $("input[name='images']")[0].files[0]);
                formData.append("auction", $("input[name=auction]").val());
                
                $.ajax({
                    url: "/reviewImage",
                    data: formData,
                    type: 'POST',
                    cache: false,
                    processData: false,
                    contentType: false,
                    error: function (data) {

                    },
                    success: function (data) {
                        $(".review-write__file--uploaded > ul").append("<li class='image-url' data-url='" + data.data + "'>" + val + "</li>");
                        // other browser 일때 input[type=file] init.
                        $("input[name=images]").val("");
                    }
                });
                break;
            default:
                $(this).val('');
                alert("gif,jpg,png 파일만 업로드 가능해요");
                break;
        }
    });

    $(".review-write-form").submit(function(e){
        e.preventDefault();
        var pictures = [];
        $('li.image-url').each(function(index) {;
            pictures.push($(this).attr("data-url"));
        });

        var params = {
            auction: $("input[name=auction]").val(),
            content: $("textarea[name=content]").val(),
            pictures: pictures
        };
        $.ajax({
            type: 'POST',
            url: '/api/reviews',
            contentType: "application/json",
            dataType: 'json',
            data: JSON.stringify(params),
            statusCode: {
                401: function(data) {
                    //alert('아이디 또는 비밀번호가 다릅니다.');
                },
                404: function(data) {
                    //alert('존재하지 않는 계정입니다.');
                }
            },
            success: function(data) {
                alert('리뷰 작성이 완료됐습니다.');
                window.location.href = "/review";
            }
        });
    });

    $(document).on('click', '.open-review-modal-btn', function(){
        $('.review-write-modal').modal();
        $("input[name=auction]").val($(this).attr("data-index"));
    });

    $(".floatmenu").click(function(){
        $(this).toggleClass("closed");

        if($(this).hasClass("closed")) {
            $(".floatmenu.button").text("Menu");
        } else {
            $(".floatmenu.button").text("Close");
        }
    });

    $(".request-select-more-a").click(function(e){
       e.preventDefault();
        $(".request-select-describe").css({
            'max-height': '100%'
        });
        $(".more").hide();
    });

    $(".request-service--input").click(function(e){
        if(typeof globalUserAuth === 'undefined') {
            alert('노쇼 예방을 위해 로그인이 필요한 서비스 입니다.');
            $('.open-login-modal').click();
            e.preventDefault();
            return false;
        }
        if(!globalUserAuth) {
            alert('노쇼 예방을 위해, 핸드폰 인증을 거친 뒤 서비스 신청이 가능합니다.');
            location.href = '/mypage';
            e.preventDefault();
            return false;
        }
    });

    $("#service-cuisines").mouseup(function(e){
        e.preventDefault();
        //$(".select-cuisines-modal").modal();
    });

    $(".select-cuisines-btn").click(function(){
        var selectValue = $("input[name=cuisines-radio]:checked").val();
        var selectText = "상관없음";
        var cuisinesArr = {};
        cuisinesArr['all'] = "상관없음";
        cuisinesArr['korean'] = "한식";
        cuisinesArr['chinese'] = "중식";
        cuisinesArr['japanese'] = "일식";
        cuisinesArr['western'] = "양식";



        if(selectValue === "etc") {
            selectValue = $("input[name=etc-cuisines]").val();
            selectText = "기타";
        } else {
            selectText = cuisinesArr[selectValue];
        }

        $('input[name=service-cuisines-hidden]').val(selectValue);
        $("input#service-cuisines").val(selectText);
        $("input#service-cuisines").attr("placeholder", "");
        $(".select-cuisines-modal").modal('hide');

    });

    $(".open-feedback-modal-a").click(function(e){
        e.preventDefault();
        $(".feedback-modal").modal();
    });

    $('select[name=service-type]').change(function(){
        var value = $(this).val();
        $(".row.service-row.pc").hide();
        $(".service-mode").hide();

        if (value === "festival") {
            value = 'ondemand';

            //$('#service-date').val(new Date());
            $('#service-budget').val('10000');
            $('#service-people').val('1');
            $('#service-zone').val(13);
            $('#service-cuisines').val('all');
            $('#service-describe').val('스마트 테크쇼 이벤트 참여!!!');
            $('#service-starttime').val(13);
        } else {
            $('#service-budget').val('');
            $('#service-people').val('');
            $('#service-zone').val('');
            $('#service-cuisines').val('all');
            $('#service-describe').val('');
            $('#service-starttime').val(1);
        }

        $(".row." + value).show();

        if($(window).width() > 768) {
            $("." + value + "-mode").show();
        }
    });

    $(".btn-feedback-submit").click(function(){
        var name = $("input[name=feedback-name]").val(),
            phone = $("input[name=feedback-phone]").val(),
            content = $("textarea[name=feedback-content]").val();
        $.ajax({
            type: 'POST',
            url: '/api/feedbacks',
            data: {
                name: name,
                phone: phone,
                content: content
            },
            statusCode: {
                401: function(data) {
                    //alert('아이디 또는 비밀번호가 다릅니다.');
                },
                404: function(data) {
                    //alert('존재하지 않는 계정입니다.');
                }
            },
            success: function(data) {
                alert('질문이 전달됐습니다. 빠르게 확인 후 답변드리겠습니다.');
                $(".feedback-modal").modal('hide');
                $("input[name=feedback-name]").val('');
                $("input[name=feedback-phone]").val('');
                //$("textarea[name=feedback-content]").val('');
            }
        });
    });
});

var timerHandle;

function startTimer(duration, display) {
    var timer = duration, minutes, seconds;
    timerHandle = setInterval(function () {
        minutes = parseInt(timer / 60, 10);
        seconds = parseInt(timer % 60, 10);

        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;

        display.text(minutes + ":" + seconds);

        if (--timer < 0) {
            clearInterval(timerHandle);
            alert('시간초과');
            $(".auth2").hide();
            $(".auth1").show();
        }
    }, 1000);
}