function loadAdminAuction(filter) {
    var jwtToken = localStorage.getItem('jwt');
    var params = {

    };
    if(filter !== 'all') {
        params.status = filter
    }
    $.ajax({
        type: 'GET',
        url: '/api/auctions',
        headers: {
            'Authorization': 'JWT ' + jwtToken
        },
        data: params,
        success: function (data) {

            var currentPage = parseInt(data.data.page);
            var nextPage = currentPage + 1;

            var $prev = $('.page-prev-a');
            var $next = $('.page-next-a');

            if(currentPage === 1) {
                $prev.hide();
            } else {
                $prev.show();
                $prev.attr('data-page', currentPage-1);
            }

            if(!data.data.hasMore) {
                $next.hide();
            } else {
                $next.show();
                $next.attr('data-page', nextPage);
            }

            if (data.data.total) {
                $.each(data.data.items, function (index, item) {
                    var statusText = "";
                    var phoneNumber = "";
                    if(typeof item.owner.phone !== "undefined") {
                        phoneNumber = item.owner.phone.number;
                    }
                    var statusSummary = "진행중";
                    if (item.status === "PROCESSING") {
                        statusText = "알맞은 공급자들에게 신청이 전달되어 입찰이 진행중입니다.";
                    } else if (item.status === "COMPLETED") {
                        statusText = "공급자 세명의 선정이 완료됐습니다, 공급자 선정 후 결제를 진행해주세요.";
                        statusSummary = "진행중";
                    } else if (item.status === "SUCCESS") {
                        statusText = "결제가 완료되었습니다, 곧 담당자로부터 연락이 갑니다. 조금만 기다려주세요.";
                        statusSummary = "결제완료"
                    } else if(item.status === "SERVICE-END") {
                        statusText = "리뷰작성전";
                        statusSummary = "리뷰작성전"
                    } else if(item.status === "REVIEW-COMPLETED") {
                        statusText = "리뷰작성완료";
                        statusSummary = "최종완료"
                    } else if(item.status === "FAILURE") {
                        statusText = "해당 경매에 입찰한 멤버스가 없습니다, 금액을 조정해보세요!";
                        statusSummary = "경매실패"
                    }
                    $('table > tbody').append(`
                      <tr>
                      <td class='col-2'>${timeSince(item.createdDate)}</td>
                      <td class='col-1'>${numberWithCommas(item.budget)}</td>
                      <td class='col-8 status'>
                      ${item.description}
                      </td>
                      <td class='col-1'>${statusSummary}</td>
                      </tr>
                      <tr class='detail-auction'>
                          <td class='col-2'></td>
                          <td class='col-1'></td>
                          <td class='col-8 status'>
                            <div class='auction-detail-wrapper'>
                                <ul>
                                    <li><span class="text-bold"></span></li>
                                    <li><span class="text-bold">이름</span>: ${item.owner.displayName}</li>
                                    <li><span class="text-bold">인원수</span>: ${item.peopleCounts}</li>
                                    <li><span class="text-bold">선호음식</span>: ${item.cuisines}</li>
                                    <li><span class="text-bold">주소</span>: ${item.serviceAddress}</li>
                                    <li><span class="text-bold">날짜</span>: ${item.serviceDate}</li>
                                    <li><span class="text-bold">전화번호</span>: ${phoneNumber}</li>
                                    <li><span class="text-bold">선택 공급자</span>: ${item.targetSuppliers.length}명</li>
                                    <li class="add-supplier">
                                        <input type='text' class='force-bid-userid ${item._id}' placeholder='추가할 유저 이메일'>
                                        <input type='text' class='force-bid-price ${item._id}' placeholder='금액'>
                                        <input type='text' class='force-bid-describe ${item._id}' placeholder='비드내용'>
                                        <input type="button" data-index='${item._id}' class="btn btn-primary force-bid-add" value="추가">
                                    </li>
                                </ul>
                                <ul class="winner-${item._id}"></ul>
                                <dl class="bid-list-dl ${item._id}">
                                    
                                </dl>
                            </div>
                           </td>
                           <td class='col-1'><input type='button' class='btn btn-primary admin-bid-list-btn' data-index='${item._id}' value='비드목록'></td>
                      </tr>
                    `);
                    if(filter === "-PROCESSING"){
                        $('.add-supplier').remove();
                        if(typeof item.winner !== 'undefined') {
                            $("ul.winner-" + item._id).append(`
                                <li><span class="text-bold">최종 성사된 공급자</span></li>
                                <li>${item.winner.owner.displayName} / 010-0000-0000 / ${numberWithCommas(item.winner.price)}</li>
                            `);
                        }
                    }
                });
            }
        }
    });
}

function toBoolean(auth) {
    return (auth === "true");
}

function loadConsumers(page) {
    var jwtToken = localStorage.getItem('jwt');
    var params = {};

    if(page > 1) {
        params.page = page;
    }

    $.ajax({
        type: 'GET',
        url: '/api/users?consumerOnly=true',
        headers: {
            'Authorization': 'JWT ' + jwtToken
        },
        data: params,
        success: function(data) {
            var currentPage = parseInt(data.data.page);
            var nextPage = currentPage + 1;
            var $prev;
            var $next;

            $prev = $('.suppliersPage-prev-a');
            $next = $('.suppliersPage-next-a');

            if(currentPage === 1) {
                $prev.hide();
            } else {
                $prev.show();
                $prev.attr('data-page', currentPage-1);
            }

            if(!data.data.hasMore) {
                $next.hide();
            } else {
                $next.show();
                $next.attr('data-page', nextPage);
            }

            $.each(data.data.items, function (index, item) {
                $('table > tbody').append(`
                    <tr class="consumer-tr-${item._id}">
                        <td><input type="text" name="displayName" value="${item.displayName}"></td>
                        <td><input type="text" name="email" value="${item.email}"></td>
                        <td><input type="text" name="phone" value="${item.phone.number}"></td>
                        <td>${item.lastLoggedInDate}</td>
                        <td><input type="button" data-index="${item._id}" class="btn btn-primary consumer-modify-btn" value="수정"></td>
                    </tr>
                `);
            });
        }
    });
}

function loadSuppliers(auth, page) {
    var jwtToken = localStorage.getItem('jwt');
    var params = {};

    if(auth !== 'all') {
        params.authenticated = toBoolean(auth);
    }

    if(page > 1) {
        params.page = page;
    }

    $.ajax({
        type: 'GET',
        url: '/api/users',
        headers: {
            'Authorization': 'JWT ' + jwtToken
        },
        data: params,
        success: function(data) {
            var currentPage = parseInt(data.data.page);
            var nextPage = currentPage + 1;
            var $prev;
            var $next;
            if(auth) {
                $prev = $('.suppliersPage-prev-a');
                $next = $('.suppliersPage-next-a');
            } else {
                $prev = $('.suppliersAuthPage-prev-a');
                $next = $('.suppliersAuthPage-next-a');
            }

            if(currentPage === 1) {
                $prev.hide();
            } else {
                $prev.show();
                $prev.attr('data-page', currentPage-1);
            }

            if(!data.data.hasMore) {
                $next.hide();
            } else {
                $next.show();
                $next.attr('data-page', nextPage);
            }

            $.each(data.data.items, function (index, item) {
                if(toBoolean(auth)) {
                    $('table > tbody').append(`
                        <tr class="supplier-tr-${item._id}">
                            <td><input name="displayName" type='text' value="${item.displayName}"></td>
                            <td>${item.role}</td>
                            <td><input name="email" type='text' value="${item.email}"></td>
                            <td><input name="phone" type='text' value="${item.phone.number}"></td>
                            <td><input name="cuisines" type='text' value="${item.supplier.cuisines}"></td>
                            <td><input name="menus" type='text' value="${item.supplier.menus}"></td>
                            <td><textarea name="introduce">${item.introduce}</textarea></td>
                            <td><input type="button" data-index="${item._id}" class="btn btn-primary supplier-modify-btn" value="수정"></td>
                        </tr>
                    `);
                } else {
                    $('table > tbody').append(`
                        <tr class="auth-tr-${item._id}">
                            <td>${item.displayName}</td>
                            <td>${item.phone.number}</td>
                            <td>${item.supplier.cuisines}</td>
                            <td>${item.supplier.menus}</td>
                            <td>${item.introduce}</td>
                            <td><input type="button" data-index="${item._id}" class="btn btn-primary supplier-auth-btn" value="인증"></td>
                        </tr>
                    `);
                }
            });
        }
    });
}

function loadFeedback() {
    var jwtToken = localStorage.getItem('jwt');
    var params = {};
    var page = 1;
    if(page > 1) {
        params.page = page;
    }

    $.ajax({
        type: 'GET',
        url: '/api/feedbacks',
        headers: {
            'Authorization': 'JWT ' + jwtToken
        },
        data: params,
        success: function(data) {
            var currentPage = parseInt(data.data.page);
            var nextPage = currentPage + 1;
            var $prev;
            var $next;

            $.each(data.data.items, function (index, item) {
                $('table > tbody').append(`
                    <tr>
                        <td>${timeSince(item.createdDate)}</td>
                        <td>${item.name}</td>
                        <td>${item.phone}</td>
                        <td>${item.content}</td>
                    </tr>
                `);
            });
        }
    });
}

$(document).on('click', '.supplier-auth-btn', function() {
    var index = $(this).attr('data-index');

    $.ajax({
        type: "PATCH",
        url: '/api/users/' + index,
        data: {
            authenticated: true,
        },
        statusCode: {
            400: function(data) {

            }
        },
        success: function() {
            alert('해당 공급자에 대한 인증이 처리되었습니다.');
            $('.auth-tr-' + index).remove();
        }
    })
});

$(document).on('click', '.consumer-modify-btn', function() {
    var index = $(this).attr('data-index');

    var params = {
        displayName: $('.consumer-tr-' + index + ' > td > input[name=displayName]').val(),
        email: $('.consumer-tr-' + index + ' > td > input[name=email]').val(),
        phone: {
            number: $('.consumer-tr-' + index + ' > td > input[name=phone]').val()
        },
    };

    $.ajax({
        type: "PATCH",
        url: '/api/users/' + index,
        contentType: "application/json",
        dataType: 'json',
        data: JSON.stringify(params),
        statusCode: {
            400: function(data) {

            }
        },
        success: function() {
            alert('해당 공급자에 대한 정보수정이 완료되었습니다.');
        }
    })
});
$(document).on('click', '.supplier-modify-btn', function() {
    var index = $(this).attr('data-index');
    var menus = $('.supplier-tr-' + index + ' > td > input[name=menus]').val().split(',');
    var cuisines = $('.supplier-tr-' + index + ' > td > input[name=cuisines]').val().split(',');

    var params = {
        displayName: $('.supplier-tr-' + index + ' > td > input[name=displayName]').val(),
        email: $('.supplier-tr-' + index + ' > td > input[name=email]').val(),
        phone: {
            number: $('.supplier-tr-' + index + ' > td > input[name=phone]').val()
        },
        introduce: $('.supplier-tr-' + index + ' > td > textarea[name=introduce]').val(),
        menus: menus,
        cuisines: cuisines
    };

    $.ajax({
        type: "PATCH",
        url: '/api/users/' + index,
        contentType: "application/json",
        dataType: 'json',
        data: JSON.stringify(params),
        statusCode: {
            400: function(data) {

            }
        },
        success: function() {
            alert('해당 공급자에 대한 정보수정이 완료되었습니다.');
        }
    })
});

$(document).on('click', '.force-bid-add', function(e) {
    var index = $(this).attr('data-index');
    var userId = $('.force-bid-userid.' + index).val();
    var price = $('.force-bid-price.' + index).val();
    var describe = $('.force-bid-describe.' + index).val();
    /*var params = {
        targetSuppliers: userId
    };
    $.ajax({
        type: 'POST',
        url: '/api/bids/' + index,
        data: params,
        statusCode: {
            400: function(data) {

            }
        },
        success: function (data) {
            alert('추가완료');
        }
    });*/

    $.ajax({
        type: 'POST',
        url: '/api/bids',
        data: {
            bidUser: userId,
            auction: index,
            price: price,
            proposal: describe
        },
        success: function (data) {
            alert('멤버스를 해당 경매에 입찰시켰습니다.');
        }
    });

});

$(document).on('click', '.admin-bid-list-btn', function(e){
    var auctionId = $(this).attr('data-index');
    var jwtToken = localStorage.getItem('jwt');
    var params = {
        auction: auctionId
    };
    $(this).remove();
    $.ajax({
        type: 'GET',
        url: '/api/bids',
        headers: {
            'Authorization': 'JWT ' + jwtToken
        },
        data: params,
        success: function(data) {
            if(data.data.total) {
                $('dl.bid-list-dl.' + auctionId).append('<dt>비드 목록(' + data.data.total + ')<dt>');
                $.each(data.data.items, function (index, item) {
                    var serviceDate = new Date(item.auction.serviceDate);
                    $('dl.bid-list-dl.' + auctionId).append(`
                            <dd>공급자: ${item.owner.displayName}, 입찰금액: ${numberWithCommas(item.price)}</dd>
                        `)
                });
            } else {
                $('dl.bid-list-dl.' + auctionId).append('<dt>비드 목록(' + 0 + ')<dt>');
                $('dl.bid-list-dl.' + auctionId).append(`
                    <dd>비딩한 멤버스가 없습니다.</dd>
                `);
            }
        }
    });
});

$(document).ready(function() {
    $('.suppliersPage-prev-a').click(function(e){
        e.preventDefault();
        var page = $(this).attr('data-page');
        $('table > tbody').html('');
        loadSuppliers('true', page);
    });

    $('.suppliersPage-next-a').click(function(e){
        e.preventDefault();
        var page = $(this).attr('data-page');
        $('table > tbody').html('');
        loadSuppliers('true', page);
    });

    $('.suppliersAuthPage-prev-a').click(function(e){
        e.preventDefault();
        var page = $(this).attr('data-page');
        $('table > tbody').html('');
        loadSuppliers('false', page);
    });

    $('.suppliersAuthPage-next-a').click(function(e){
        e.preventDefault();
        var page = $(this).attr('data-page');
        $('table > tbody').html('');
        loadSuppliers('false', page);
    });
    var a = function b() {
        console.log('dddd');
    };
    a();
});