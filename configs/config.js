'use strict';

var _ = require('lodash');

module.exports = _.merge(
    require('./environments/all.js'),
    require('./environments/' + process.env.NODE_ENV + '.js') || {}
);
