'use strict';

var path = require('path');
var rootPath = path.normalize(__dirname + '/../..');

module.exports = {
    root: rootPath,
    port: process.env.PORT || 3000,
    sessionSecretKey: "alskdWAE%J#$IJFKLerwioptjeopir",
    locales: ['ko_KR', 'en_US'],
    defaultLocale: 'en_US',
    administrators: ['ADMINISTRATOR'], 
    suppliers: ['CHEF', 'FOOD-TRUCK', 'CATERING'],
    spaceSuppliers: ['EMPTY-FOOD-TRUCK'],
    consumers: ['CONSUMER'],
    auctionStatus: {
        processing: 'PROCESSING',
        success: 'SUCCESS',
        failure: 'FAILURE',
        completed: 'COMPLETED',
        serviceEnd: 'SERVICE-END',
        reviewCompleted: 'REVIEW-COMPLETED'
    },
    subscriptionStatus: {
        processing: 'PROCESSING',
        success: 'SUCCESS',
        failure: 'FAILURE',
    },
    pagination: {
        limit: 10
    },

    payType: {
        entire: {
            payType: '0',
            payMethod: '',
        },
        creditCard: {
            payType: '1',
            payMethod: 'CARD',
        },
        unknown: {
            payType: '2',
            payMethod: '',
        },
        unknown: {
            payType: '3',
            payMethod: '',
        },
        keyIn: {
            payType: '4',
            payMethod: 'KEYIN',
        },
        unknown: {
            payType: '5',
            payMethod: '',
        },
        transfering: {
            payType: '6',
            payMethod: 'BANK',
        },
        virtualAccount: {
            payType: '7',
            payMethod: 'VBANK',
        },
        creditCardAndVirtualAccount: {
            payType: '8',
            payMethod: 'CARD',
        },
        mobilePayment: {
            payType: '9',
            payMethod: 'CELLPHONE',
        },
    }
};
