'use strict';

var config = require('./config');
var i18n = require('i18n');

module.exports = function(app) {
    i18n.configure({
        locales: config.locales,
        defaultLocale: config.defaultLocale,
        cookie: 'locale',
        directory: config.root + '/locales'
    });
    app.use(i18n.init);
};
