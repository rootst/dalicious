'use strict';
var JwtStrategy = require('passport-jwt').Strategy;
var FacebookStrategy = require('passport-facebook').Strategy;
var KakaoStrategy = require('passport-kakao').Strategy;
var ExtractJwt = require('passport-jwt').ExtractJwt;
var _ = require('lodash');
var flash = require('connect-flash');

var config = require('./config');
var User = require(config.root + '/models/user');


module.exports = function(app, passport) {
    var jwtOptions = {};
    jwtOptions.secretOrKey = config.sessionSecretKey;
    jwtOptions.jwtFromRequest = ExtractJwt.fromAuthHeader();
    passport.use(new JwtStrategy(jwtOptions, (jwtPayload, done) => {
        User.findOne({_id: jwtPayload._id}).exec((errorResult, result) => {
            if (errorResult) {
                errorResult._status = 500;
                return done(errorResult, false);
            }

            if (!result) {
                var errorResult = {};
                errorResult._status = 404;
                return done(errorResult, false);
            }
            return done(null, result);
        });
    }));

    var facebookOptions = {};
    facebookOptions.clientID = config.facebook.clientId;
    facebookOptions.clientSecret = config.facebook.clientSecret;
    facebookOptions.callbackURL = config.facebook.callbackUrl;
    facebookOptions.profileFields = config.facebook.profileFields;

    passport.use(new FacebookStrategy(facebookOptions, (accessToken, refreshToken, profile, done) => {
        // TIP FOR BOOST UP PERFORMANCE
        // SHOULD BE TEST
        // process.nextTick(function() {});

        // CELLPHONE authentication check
        User.findOne({'facebook.id' : profile.id}).then((result) => {
            if (!result) {
                if (_.isUndefined(profile.emails)) { // phone authentication case
                    var newUser = new User();
                    newUser.facebook = {};
                    newUser.facebook.id = profile.id;
                    newUser.facebook.accessToken = accessToken;
                    newUser.email = 'facebook_' + profile.accessToken;
                    newUser.avatar = profile.photos[0].value;
                    newUser.role.push('CONSUMER');
                    newUser.save((errorResult) => {
                        if (errorResult) {
                            errorResult._status = 500;
                            return done(errorResult, false);
                        }
                        return done(null, newUser);
                    });

                } else {
                    User.findOne({email: profile.emails[0].value}).then((result) => {
                        if (result) {
                            return done(null, false, { message: '해당 계정의 이메일은 이미 가입되어있어 해당 페이스북 계정으로 가입을 진행할 수 없습니다.' });
                        }
                        
                        var newUser = new User();
                        newUser.facebook = {};
                        newUser.facebook.id = profile.id;
                        newUser.facebook.accessToken = accessToken;
                        newUser.email = profile.emails[0].value;
                        newUser.displayName = profile.name.givenName + '_' + profile.name.familyName;
                        newUser.role.push('CONSUMER');
                        newUser.save((errorResult) => {
                            if (errorResult) {
                                errorResult._status = 500;
                                return done(errorResult, false);
                            }
                            return done(null, newUser);
                        });
                        
                    }).catch((errorResult) => {
                        if (errorResult) {
                            errorResult._status = 500;
                            return done(errorResult, false);
                        }
                    });
                }

            } else { // should be. because of async.
                return done(null, result);
            }

        }).catch((errorResult) => {
            if (errorResult) {
                errorResult._status = 500;
                return done(errorResult, false);
            }
        });
    }));

    var kakaoOptions = {};
    kakaoOptions.clientID = config.kakaotalk.clientId;
    kakaoOptions.callbackURL = config.kakaotalk.callbackUrl;

    passport.use(new KakaoStrategy(kakaoOptions, (accessToken, refreshToken, profile, done) => {
        // TIP FOR BOOST UP PERFORMANCE
        // SHOULD BE TEST
        // process.nextTick(function() {});

        // CELLPHONE authentication check
        console.log(profile);
        User.findOne({'kakao.id' : profile.id}).then((result) => {
            if (!result) {
                var newUser = new User();
                newUser.kakao = {};
                newUser.kakao.id = profile.id;
                newUser.displayName = profile.displayName;
                newUser.email = 'kakaotalk_' + accessToken;
                newUser.avatar = profile._json.properties.profile_image;
                newUser.role.push('CONSUMER');
                newUser.save((errorResult) => {
                    if (errorResult) {
                        errorResult._status = 500;
                        return done(errorResult, false);
                    }
                    return done(null, newUser);
                });

            } else { // should be. because of async.
                return done(null, result);
            }

        }).catch((errorResult) => {
            if (errorResult) {
                errorResult._status = 500;
                return done(errorResult, false);
            }
        });
    }));
    
    passport.serializeUser((user, done) => {
        done(null, user._id);
    });
    
    passport.deserializeUser((id, done) => {
        User.findById(id, (err, user) => {
            done(err, user);
        });
    });
    
    app.use(passport.initialize());
    app.use(passport.session());
    app.use(flash());
};
