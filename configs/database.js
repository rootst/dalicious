'use strict';
var mongoose = require('mongoose');

var config = require('./config');
var isDebug = require(config.root + '/utilities/isDebug');


module.exports = function(callback) {
    mongoose.Promise = require('bluebird');
    mongoose.connect('mongodb://dalicious:dalicious@localhost/dalicious');
    mongoose.set("debug", isDebug());

    var db = mongoose.connection;
    db.on('error', console.error.bind(console, 'connection error:'));
    db.once('open', callback);
};
