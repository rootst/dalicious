'use strict';

var path = require('path');
var express = require('express');
var morgan = require('morgan'); // express log
var bodyParser = require('body-parser'); // express json, express urlencoded
var cookieParser = require('cookie-parser'); // express cookieParser
var session = require('express-session'); // express session
var methodOverride = require('method-override'); // express method-override
var favicon = require('static-favicon'); // express favicon
var errorHandler = require('errorhandler'); // express errorHandler
var compression = require('compression');
var cors = require('cors');
var jade = require('jade');
var babel = require('jade-babel');
var multer  = require('multer');
var redis = require('redis'); // redis

var config = require('./config');
var isDebug = require(config.root + '/utilities/isDebug');


module.exports = function(app) {
    if (isDebug()) {
        //app.use(require('connect-livereload')());

        // Disable caching of scripts for easier testing
        app.use((request, response, next) => {
            if (request.url.indexOf('/js/') === 0) {
                response.header('Cache-Control', 'no-cache, no-store, must-revalidate');
                response.header('Pragma', 'no-cache');
                response.header('Expires', 0);
            }
            next();
        });

        //app.use(express.static(path.join(config.root, '.tmp')));
        app.use(express.static(path.join(config.root, 'public')));
        app.set('views', config.root + '/public/views');

    } else {
        app.use(favicon(path.join(config.root, '.dist/images', 'favicon.ico')));
        app.use(express.static(path.join(config.root, '.dist')));
        app.use(express.static(path.join(config.root, 'node_modules')));
        app.set('views', config.root + '/.dist/views');
    }

    app.locals.pretty = true;
    app.set('view engine', 'jade');
    jade.filters.babel = babel({});

    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: false }));
    app.use(compression());
    app.use(cookieParser());
    app.use(cors());
    app.use(morgan('dev'));
    app.use(methodOverride()); // simulate DELETE and PUT
    app.use(methodOverride('X-HTTP-Method')); // Microsoft
    app.use(methodOverride('X-HTTP-Method-Override')); // Google/GData
    app.use(methodOverride('X-Method-Override')); // IBM
    app.use(multer({ dest: './.uploads/'}).any());

    if (isDebug()) {
        app.use(session({
            secret: config.sessionSecretKey,
            resave: false,
            saveUninitialized: true
        }));
    } else {
        var redisStore = require('connect-redis')(session);
        var client = redis.createClient(config.redis.port, config.redis.url);
        app.use(session({
            secret: config.sessionSecretKey,
            store: new redisStore({
                host: config.redis.url,
                port: 6379,
                client: client,
                prefix : 'session',
                db : 0
            }),
            saveUninitialized: true, // don't create session until something stored,
            resave: true // don't save session if unmodified
        }));
    }

    // Error handler
    if (isDebug()) {
        app.use(errorHandler());
    }
};
