'use strict';

var assert = require('assert');

describe('BDD style', function() {
    before(function() {
        // excuted before test suite
    });

    after(function() {
        // excuted after test suite
    });

    beforeEach(function() {
        // excuted before every test
    });

    afterEach(function() {
        // excuted after every test
    });

    describe('RegExp', function() {
        it('RegExp test', function() {
            var regex = new RegExp("(((대한민국|한국)?[가-힣]+(시|도)|[서울]|[인천]|[대구]|[광주]|[부산]|[울산])( |)([가-힣]+(시|군|구)))")
            var extractAddresses = regex.exec("대한민국 부산광역시 연산구 연산동");
            console.log(extractAddresses);
            assert(true);
        });

        it('Bid deletion test', function() {

        });

        it('Bid update test', function() {
            
        });
    });
});
