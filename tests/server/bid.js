'use strict';

var assert = require('assert');

describe('BDD style', function() {
    before(function() {
        // excuted before test suite
    });

    after(function() {
        // excuted after test suite
    });

    beforeEach(function() {
        // excuted before every test
    });

    afterEach(function() {
        // excuted after every test
    });

    describe('Bid', function() {
        var Bid = require('./../../models/bid');
        var Auction = require('./../../models/auction');
        var User = require('./../../models/user');

        it('Bid creation test', function() {
            var user = new User();
            user.email = "test@test.com";
            user.password = "test";
            user.role = "CHEF";

            var auction = new Auction();
            auction.owner = user;
            auction.description = "Sample Description";

            var bid = new Bid();
            bid.owner = user;
            bid.auction = auction;
            
            console.log(JSON.stringify(bid));
            assert(true);
        });

        it('Bid deletion test', function() {

        });

        it('Bid update test', function() {
            
        });
    });
});
