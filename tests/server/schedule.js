'use strict';

var assert = require('assert');

describe('BDD style', function() {
    before(function() {
        // excuted before test suite
    });

    after(function() {
        // excuted after test suite
    });

    beforeEach(function() {
        // excuted before every test
    });

    afterEach(function() {
        // excuted after every test
    });

    describe('NodeSchduleTest', function() {
        var Auction = require('./../../models/auction');
        var User = require('./../../models/user');

        it('NodeSchedule test', function() {
            var user = new User();
            user.email = "test@test.com";
            var processedFields = {};
            processedFields.owner = user;
            processedFields.peopleCounts = 10;
            processedFields.budget = 10000000;
            processedFields.serviceDate = new Date(2017, 1, 20);
            processedFields.serviceAddress = "서울시";
            processedFields.cuisines = "chinese";
            processedFields.description = "description";
            processedFields.status = "PROCESSING";
            var auction = new Auction(processedFields);
            auction.save((errorResult, result) => {
                if (errorResult) {
                    errorResult._status = 500;
                    return reject(errorResult);
                }

                var startDate = new Date(result.serviceDate);
                var triggerDate = startDate;
                triggerDate.setDate(startDate.getSeconds() + 10);
                var j = schedule.scheduleJob(triggerDate, function(auctionData) {
                    auctionData.status = "COMPLETED";
                    auctionData.save((errorResult, result) => {
                        if (errorResult) {
                            console.error(errorResult);
                            return;
                        }
                        console.log("asdfasdf");
                    });
                }.bind(null, result));
                return resolve(result);
            });
            
            //console.log(JSON.stringify());
            assert(true);
        });
    });
});
