var pump = require('pump'); // for error log easier
var gulp = require('gulp');
var webserver = require('gulp-webserver');
var concat = require('gulp-concat');
var rename = require('gulp-rename');
var babel = require('gulp-babel'); // for compile es6
var uglify = require('gulp-uglify');
var sass = require('gulp-sass');
var livereload = require('gulp-livereload');

var src = 'public';
var dist = '.dist';

var paths = {
	jsVendor: src + '/js/vendor/**/*.js',
	jsOwn: src + '/js/0_main/**/*.js',
	scss: src + '/scss/**/*.scss',
	jade: src + '/views/**/*.jade',
	font: src + '/fonts/*',
	image: src + '/images/**/*',
};
// 웹서버를 localhost:8000 로 실행한다.
gulp.task('server', function (cb) {
	pump([
		gulp.src(dist + '/'),
		webserver()
	], cb);
});

// 자바스크립트 파일을 하나로 합치고 압축한다.
gulp.task('combine-js', function (cb) {
	pump([
		gulp.src(paths.jsOwn),
		//concat('script.js'),
		gulp.dest(dist + '/js/0_main'),
		babel({presets: ['es2015']}),
		//rename('script.min.js'),
		uglify(),
		gulp.dest(dist + '/js/0_main')
	], cb);
});

// sass 파일을 css 로 컴파일한다.
gulp.task('compile-sass', function (cb) {
	pump([
		gulp.src(paths.scss),
		sass(),
		gulp.dest(dist + '/css')
	], cb);
});

// jade 파일을 카피한다.
gulp.task('copy-jade', function (cb) {
	pump([
		gulp.src(paths.jade),
		gulp.dest(dist + '/views')
	], cb)
});

// font 파일을 카피한다.
gulp.task('copy-font', function (cb) {
	pump([
		gulp.src(paths.font),
		gulp.dest(dist + '/fonts')
	], cb)
});

// javascript 파일을 카피한다.
gulp.task('copy-javascript', function (cb) {
	pump([
		gulp.src(paths.jsVendor),
		gulp.dest(dist + '/js/vendor')
	], cb)
});

// images 파일을 카피한다.
gulp.task('copy-images', function (cb) {
	pump([
		gulp.src(paths.image),
		gulp.dest(dist + '/images')
	], cb)
});

// 파일 변경 감지 및 브라우저 재시작
gulp.task('watch', function () {
	livereload.listen();
	gulp.watch(paths.js, ['combine-js']);
	gulp.watch(paths.scss, ['compile-sass']);
	gulp.watch(dist + '/**').on('change', livereload.changed);
});

//기본 task 설정
gulp.task('default', [
	'server', 'combine-js', 
	'compile-sass',
	'watch' ]);

//기본 task 설정
gulp.task('build', [
	'combine-js',
	'copy-javascript',
	'compile-sass',
	'copy-jade',
	'copy-font',
	'copy-images'
]);