'use strict';
var _ = require('lodash');

var config = require('./../configs/config');


exports = module.exports = function(target, source) {
    var targetArray = [];
    var sourceArray = [];

    if (!_.isArray(target)) {
        targetArray = [target];
    } else {
        targetArray = target;
    }
    if (!_.isArray(source)) {
        sourceArray = [source];
    } else {
        sourceArray = source;
    }

    var i = sourceArray.length;
    while (i-- && (i >= 0)) {
        var j = targetArray.length;
        while (j-- && (j >= 0)) {
            if (String(targetArray[j]) === String(sourceArray[i])) {
                return true;
            }
        }
    }
    return false;
}