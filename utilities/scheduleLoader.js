'use strict';
var _ = require('lodash');
var schedule = require('node-schedule');

var config = require('./../configs/config');
var Aws = require(config.root + '/utilities/aws');
var Auction = require(config.root + '/models/auction');
var Bid = require(config.root + '/models/bid');


exports.loadAuctionProcessing = function() {
    Auction.find({status: config.auctionStatus.processing}).then((auctionsResults) => {
        _.forEach(auctionsResults, (auctionResult) => {
            // first of all, lets find service already finished.
            var auctionDate = new Date(auctionResult.createdDate);
            var limitDate = new Date(auctionDate);
            limitDate.setDate(auctionDate.getDate() + 1);
            var currentDate = new Date();

            if (currentDate > limitDate) {
                // already finished.
                auctionResult.status = config.auctionStatus.completed;
                // find TOP3
                // sort, limit 3
                Bid.find({auction: auctionResult, price: {$lte: auctionResult.budget}}).sort({price: -1}).limit(3).then((result) => {
                    _.forEach(result, (bid) => {
                        auctionResult.chosenBids.push(bid);
                    });

                    if (_.isEmpty(result)) {
                        auctionResult.status = config.auctionStatus.failure;
                    }

                    auctionResult.save((saveErrorResult, saveResult) => {
                        if (saveErrorResult) {
                            console.error(saveErrorResult);
                            return;
                        }
                    });
                });
                return;
            }

            // next, normal case

            var triggerDate = new Date();
            triggerDate.setDate(auctionDate.getDate() + 1);
            var j = schedule.scheduleJob(triggerDate, function(auctionResult) {
                auctionResult.status = config.auctionStatus.completed;

                // find TOP3
                // sort, limit 3
                Bid.find({auction: auctionResult, price: {$lte: auctionResult.budget}}).sort({price: -1}).limit(3).then((result) => {
                    _.forEach(result, (bid) => {
                        auctionResult.chosenBids.push(bid);
                    });

                    if (_.isEmpty(result)) {
                        auctionResult.status = config.auctionStatus.failure;
                    }

                    auctionResult.save((saveErrorResult, saveResult) => {
                        if (saveErrorResult) {
                            console.error(saveErrorResult);
                            return;
                        }
                    });
                });
            }.bind(null, auctionResult));
        });
    });
};

exports.loadAuctionServiceEnd = function() {
    var options = {
        populate: [{path:'owner'}]
    };

    Auction.find({status: config.auctionStatus.success}, options).then((auctionsResults) => {
        _.forEach(auctionsResults, (auctionResult) => {
            // first of all, lets find service already finished.
            var limitDate = new Date(auctionResult.serviceStartTime);
            var currentDate = new Date();

            if (currentDate > limitDate) {
                // already finished.
                auctionResult.status = config.auctionStatus.serviceEnd;
                auctionResult.save((saveError, saved) => {
                    if (saveError) {
                        console.error(saveError);
                        return;
                    }

                    var phoneNumber = saved.owner.phone.number;
                    var messageContent = '';
                    messageContent = 'Dalicious입니다. 서비스가 시작되셨나요? 서비스 후 후기를 작성해주세요.';
                    Aws.publishSMS(
                        messageContent,
                        'string',
                        phoneNumber,
                        (sendError, sent) => {
                            if (sendError) {
                                console.error(sendError);
                                return;
                            }

                            console.log("완료");
                        }
                    );
                });
                return;
            }

            // next, normal case

            var j = schedule.scheduleJob(auctionResult.serviceStartTime, function(auctionResult) {
                auctionResult.status = config.auctionStatus.serviceEnd;
                auctionResult.save((saveError, saved) => {
                    if (saveError) {
                        console.error(saveError);
                        return;
                    }

                    var phoneNumber = saved.owner.phone.number;
                    var messageContent = '';
                    messageContent = 'Dalicious입니다. 서비스가 시작되셨나요? 서비스 후 후기를 작성해주세요.';
                    Aws.publishSMS(
                        messageContent,
                        'string',
                        phoneNumber,
                        (sendError, sent) => {
                            if (sendError) {
                                console.error(sendError);
                                return;
                            }

                            console.log("완료");
                        }
                    );
                });
            }.bind(null, auctionResult));
        });
    });
};