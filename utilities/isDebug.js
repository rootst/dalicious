'use strict';
var config = require('../configs/config');

module.exports = function() {
    return config.env === 'development';
};
