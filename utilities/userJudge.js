'use strict';
var _ = require('lodash');

var config = require('./../configs/config');


function UserJudge() {
    this._userRoles = _.concat(config.administrators, config.suppliers, config.spaceSuppliers, config.consumers);
}

UserJudge.prototype._matchRole = function(roleNames) {
    var ret = false;
    _.forEach(roleNames, (roleName) => {
        if (_.indexOf(this._userRoles, roleName) !== -1) {
            ret = true;
        }
    });
    return ret;
}

UserJudge.prototype.requireRole = function(roleNames) {
    if (!_.isArray(roleNames)) {
        roleNames = [roleNames];
    }

    if (this._matchRole(roleNames) === false) {
        return function(request, response, next) {
            var errorResult = {};
            errorResult._status = 403;
            next(errorResult);
        };
    }
    var thisObject = this;
    return function(request, response, next) {
        if (thisObject._matchRole(request.user.role) === true) {
            next();
        } else {
            var errorResult = {};
            errorResult._status = 403;
            next(errorResult);
        }
    };
};

exports = module.exports = new UserJudge();
