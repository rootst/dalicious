'use strict';
var _ = require('lodash');

var config = require('../configs/config');
var User = require(config.root + '/models/user');


exports.verifyMobile = function(request, response, next) {
    if (_.isUndefined(request.user)) {
        var errorResult = {};
        errorResult._status = 404;
        return next(errorResult);
    }

    if (!request.user.phone.verified) {
        var errorResult = {};
        errorResult._status = 403;
        errorResult._message = "휴대폰 인증이 필요합니다.";
        return next(errorResult);
    }

    next();
};
