'use strict';
var _ = require('lodash');
var RtmClient = require('@slack/client').RtmClient;
var CLIENT_EVENTS = require('@slack/client').CLIENT_EVENTS;

var config = require('../configs/config');
var isDebug = require(config.root + '/utilities/isDebug');


function Slack() {
  this.bot_token = config.slack.token;
  this.rtm = new RtmClient(this.bot_token);
  this.channel;

  // The client will emit an RTM.AUTHENTICATED event on successful connection, with the `rtm.start` payload 
  this.rtm.on(CLIENT_EVENTS.RTM.AUTHENTICATED, (rtmStartData) => {
    for (const c of rtmStartData.channels) {
        if (c.is_member && c.name ==='notification') { this.channel = c.id }
    }
    console.log(`Logged in as ${rtmStartData.self.name} of team ${rtmStartData.team.name}, but not yet connected to a channel`);
  });

  // you need to wait for the client to fully connect before you can send messages 
  this.rtm.on(CLIENT_EVENTS.RTM.RTM_CONNECTION_OPENED, function () {
  });
  this.rtm.start();
}

Slack.prototype.sendMessage = function(message) {
  this.rtm.sendMessage(message, this.channel);
}

exports = module.exports = new Slack();