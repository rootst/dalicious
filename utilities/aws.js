'use strict';
var _ = require('lodash');
var AWS = require('aws-sdk');

var config = require('../configs/config');
var isDebug = require(config.root + '/utilities/isDebug');


function Aws() {

}

Aws.prototype._processPhoneNumber = function(phoneNumber) {
    // check if already processed
    if (phoneNumber.charAt(0) === '+') {
        return phoneNumber;
    }

    // remove first 0 on number
    if (phoneNumber.charAt(0) === '0') {
        phoneNumber = phoneNumber.substr(1);
    }
    return '+82-' + phoneNumber;
}

Aws.prototype.publishSMS = function(message, messageStructure, phoneNumber, callback) {
    if (isDebug()) {
        return callback(null, null);
    }
    if (!_.isFunction(callback) || _.isUndefined(phoneNumber)) {
        return callback('Arguments error', null);
    }

    // NOTE: 리전이 sms의 경우 ap-northeast-2로 안되는 바람에 이렇게 강제로 넣어야 합니다.
    AWS.config.region = config.aws.smsRegion;
    AWS.config.accessKeyId = config.aws.accessKeyId;
    AWS.config.secretAccessKey = config.aws.secretAccessKey;
    let SNS = new AWS.SNS();

    let smsParameter = {
        Message: message,
        MessageStructure: messageStructure,
        PhoneNumber: this._processPhoneNumber(phoneNumber)
    };
    SNS.publish(smsParameter, (smsError, sms) => {
        if (smsError) {
            return callback(smsError, null);
        }
        callback(null, sms);
    });
};

Aws.prototype.imageUpload = function(bucket, key, acl, file, callback) {
    if (!_.isFunction(callback)) {
        throw('Arguments error');
    }

    // NOTE: 리전이 sms의 경우 ap-northeast-2로 안되는 바람에 이렇게 강제로 넣어야 합니다.
    AWS.config.region = config.aws.region;
    AWS.config.accessKeyId = config.aws.accessKeyId;
    AWS.config.secretAccessKey = config.aws.secretAccessKey;
    let S3 = new AWS.S3();

    let s3Parameter = {
        Bucket: bucket,
        Key: key,
        ACL: acl,
        ContentType: file.mimetype,
        Expires: 2592000,
        Body: require('fs').createReadStream(config.root + '/.uploads/' + file.filename)
    };
    S3.upload(s3Parameter, (uploadError, uploaded) => {
        if (uploadError) {
            uploadError._status = 500;
            return callback(uploadError, null);
        }

        callback(null, uploaded);
    });
};

Aws.prototype.publishPush = function(callback) {
    if (!_.isFunction(callback)) {
        throw('Arguments error');
    }

    // NOTE: 리전이 sms의 경우 ap-northeast-2로 안되는 바람에 이렇게 강제로 넣어야 합니다.
    AWS.config.region = config.aws.region;
    AWS.config.accessKeyId = config.aws.accessKeyId;
    AWS.config.secretAccessKey = config.aws.secretAccessKey;
    let SNS = new AWS.SNS();

    let smsParameter = {
        Message: message,
        MessageStructure: messageStructure,
        PhoneNumber: phoneNumber
    };
    SNS.createPlatformEndpoint({
        PlatformApplicationArn: '{APPLICATION_ARN}',
        Token: '{DEVICE_TOKEN}'
    }, (creationError, created) => {
        if (creationError) {
            return callback(creationError, null);
        }

        let endPointArn = created.EndpointArn;
        let payload = {
            default: 'Test',
            APNS: {
                aps: {
                    alert: 'Test',
                    sound: 'default',
                    badge: 1
                }
            },
            GCM: {
                data: {
                    message: 'Test'
                }
            }
        };
        // first have to stringify the inner APNS object...
        payload.APNS = JSON.stringify(payload.APNS);
        // first have to stringify the inner GCM object...
        payload.GCM = JSON.stringify(payload.GCM);
        // then have to stringify the entire message payload
        payload = JSON.stringify(payload);

        let pushParameter = {
            Message: payload,
            MessageStructure: 'json',
            TargetArn: endPointArn
        };
        SNS.publish(pushParameter, (pushError, push) => {
            if (pushError) {
                return callback(pushError, null);
            }

            callback(null, push);
        });
    });
}

exports = module.exports = new Aws();