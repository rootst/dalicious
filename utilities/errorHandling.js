'use strict';

module.exports = function(app) {
    app.use(function(errorResult, request, response, next) {
        if (!errorResult) {
            return next(); // you also need this line
        }
        console.error(errorResult);
        response.status(errorResult._status).json({
            data: errorResult._message
        });
    });
};
