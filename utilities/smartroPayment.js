'use strict';
const _ = require('lodash');
const crypto = require('crypto');
const http = require('http');
const iconv = require('iconv').Iconv;

const config = require('./../configs/config');


function Smartro() {
    // NOTE : DO NOT CHANGE. FIT WITH SMARTRO.
    this.formData = {
        EncryptData : '', // 암호화시킨 코드
        GoodsCnt : '', // 상품 갯수
        GoodsName : '', // 상품명
        Amt : '', // 상품 가격
        GoodsURL : '', // ???
        Moid : '', // 상품 주문 번호
        MID : '', // 회원사 아이디
        ReturnURL : '', // 결제결과전송 URL
        ResultYN : '', // 결제결과창 유무
        RetryURL : '', // 결제결과 다시시도 URL
        mallUserID : '', // 회원사 고객 아이디
        BuyerName : '', // 구매자 명
        BuyerTel : '', // 구매자 연락처
        BuyerEmail : '', // 구매자 이메일
        ParentEmail : '', // 보호자 이메일
        BuyerAddr : '', // 배송지 주소
        BuyerPostNo : '', // 배송지 우편번호
        MallIP : '', // 메일 IP
        VbankExpDate : '', // 가상계좌 입금기한
        BrowserType : '', // 브라우저 타입 (MSIE 7 : ??, MSIE 6 : ??)
        MallReserved : '', // 상점 예비정보
        SUB_ID : '', // 서브몰 아이디
        EncodingType : '', // 인코딩 타입 (utf8 : UTF-8, euckr : EUC-KR)
        GoodsCl : '', // 결제구분 (1 : 실물, 0 : 컨텐츠)
        OpenType : '', // 오픈타입 (KR : 한글, EN : 영어)
        SocketYN : '', // Adaptor 모듈 사용 여부(N : Weblink, Y : Adaptor)
        EncodeParameters : '', // 인코딩 파라미터
        SkinColor : '', // 스킨컬러 ("" : 기본, VIOLET : 보라, BLUE : 블루, GREEN : 그린, RED : 레드, YELLOW : 옐로우)
        PayMethod : '', // 결제 방법
        payType : '' // 결제 타입
    };
}

Smartro.prototype._encodeMD5HexBase64 = function(key) {
    let md5Key = crypto.createHash('md5').update(key).digest('hex');
    return new Buffer(md5Key).toString('base64');
};

Smartro.prototype._encodeMD5Base64 = function(key) {
    let md5Key = crypto.createHash('md5').update(key);
    return new Buffer(md5Key).toString('base64');
};

Smartro.prototype._pad = function(number, width) {
    number = number + '';
    let arr = new Array(width - number.length + 1);
    arr = _.join(arr, '0');
    return number.length >= width ? number : arr + number;
}

Smartro.prototype._getDateFormatyyyyMMddHHmmss = function() {
    let now = new Date();
    let year = this._pad(now.getFullYear(), 4);
    let month = this._pad(now.getMonth(), 2);
    let day = this._pad(now.getDate(), 2);
    let hour = this._pad(now.getHours(), 2);
    let minute = this._pad(now.getMinutes(), 2);
    let second = this._pad(now.getSeconds(), 2);

    return year + month + day + hour + minute + second;
};

Smartro.prototype._readJSONResponse = function(response) {
    var responseData = '';
    response.on('data', function (chunk) {
        responseData += chunk;
    });
    response.on('end', function () {
        var dataObj = JSON.parse(responseData);
        console.log("Raw Response: " +responseData);
        console.log("Message: " + dataObj.message);
        console.log("Question: " + dataObj.question);
    });
}

Smartro.prototype.pay = function(price) {
    let requestDate = this._getDateFormatyyyyMMddHHmmss();
    let key = requestDate + config.payment.merchantId + price.toString(10) + config.payment.merchantKey;

    let encryptedData = this._encodeMD5HexBase64(key);

    const options = {
        host: config.payment.url.host,
        path: config.payment.url.pay,
        port: config.payment.port,
        method: 'POST'
    };
    let httpRequest = http.request(options, this._readJSONResponse);
    httpRequest.write(JSON.stringify(this.formData), () => {
        httpRequest.end();
    });
}

exports = module.exports = new Smartro();
