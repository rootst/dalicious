'use strict';
var _ = require('lodash');

var config = require('../../../configs/config');
var stringContains = require(config.root + '/utilities/stringContains');
var Subscription = require(config.root + '/models/subscription');


exports.updateItem = function(request, response, next) {
    var pk = request.params.pk;
    var user = request.user;
    var fields = request.body;

    Subscription.findById(pk, (errorResult, result) => {
        if (errorResult) {
            errorResult._status = 500;
            return next(errorResult);
        }
        if (!result) {
            var errorResult = {};
            errorResult._status = 404;
            return next(errorResult);
        }
        // check authenticate
        if ((String(result.owner) !== String(user._id)) && (!stringContains(user.role, config.administrators))) {
            var errorResult = {};
            errorResult._status = 403;
            return next(errorResult);
        }

        _.forOwn(fields, (value, key) => {
            if (key === 'winner') {
                result.status = config.subscriptionStatus.success
            }

            if (_.isArray(result[key])) {
                if (key === 'targetSuppliers') {
                    var dictValue = {};
                    dictValue.bidable = true;
                    dictValue.user = value;
                    result[key].push(dictValue);
                } else {
                    result[key].push(value);
                }
            } else {
                result[key] = value;
            }
        });

        result.save((updateErrorResult, updateResult) => {
            if (updateErrorResult) {
                updateErrorResult._status = 500;
                return next(updateErrorResult);
            }
            if (!updateResult) {
                var updateErrorResult = {};
                updateErrorResult._status = 500;
                return next(updateErrorResult);
            }

            request._subscriptionSchema = updateResult;
            next();
        });
    });
};

exports.sendResponse = function(request, response, next) {
    response.status(200).json({data: request._subscriptionSchema._doc});
    next();
};