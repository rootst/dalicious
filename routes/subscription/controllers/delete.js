'use strict';
var config = require('../../../configs/config');
var stringContains = require(config.root + '/utilities/stringContains');
var Subscription = require(config.root + '/models/subscription');


exports.deleteItem = function(request, response, next) {
    var pk = request.params.pk;
    var user = request.user;

    Subscription.findById(pk, (errorResult, result) => {
        if (errorResult) {
            errorResult._status = 500;
            return next(errorResult);
        }
        if (!result) {
            var errorResult = {};
            errorResult._status = 404;
            return next(errorResult);
        }
        // check authenticate
        if ((String(result.owner) !== String(user._id)) && (!stringContains(user.role, config.administrators))) {
            var errorResult = {};
            errorResult._status = 403;
            return next(errorResult);
        }

        result.remove((removeErrorResult, removeResult) => {
            if (removeErrorResult) {
                removeErrorResult._status = 500;
                return next(removeErrorResult);
            }

            next();
        });
    });
};

exports.sendResponse = function(request, response, next) {
    response.status(204).json({});
    next();
};