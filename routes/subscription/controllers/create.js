'use strict';
var _ = require('lodash');

var config = require('../../../configs/config');
var Subscription = require(config.root + '/models/subscription');
var Slack = require(config.root + '/utilities/slack');
var Aws = require(config.root + '/utilities/aws');


exports.createItem = function(request, response, next) {
    var fields = request.body;
    var user = request.user;

    var processedFields = {};
    processedFields.owner = user;
    processedFields.peopleCounts = fields.peopleCounts;
    //processedFields.serviceDate = fields.serviceDate;
    processedFields.serviceAddress = fields.serviceAddress;
    //processedFields.cuisines = fields.cuisines;
    processedFields.team = fields.team;
    processedFields.description = fields.description;
    processedFields.status = config.subscriptionStatus.processing;
    var subscription = new Subscription(processedFields);
    subscription.save((errorResult, result) => {
        if (errorResult) {
            errorResult._status = 500;
            return next(errorResult);
        }
        if (!result) {
            var errorResult = {};
            errorResult._status = 500;
            return next(errorResult);
        }

        request._subscriptionSchema = result;
        next();
    });
};

exports.generateSMS = function(request, response, next) {
    var phoneNumber = request.user.phone.number;
    var messageContent = '';

    messageContent = 'Dalicious입니다. 귀하의 정기 서비스 신청이 완료되었습니다.';

    Aws.publishSMS(
        messageContent,
        'string',
        phoneNumber,
        (sendError, sent) => {
            if (sendError) {
                return next(sendError);
            }
            Slack.sendMessage('공급자가 회원가입하였습니다.');
            next();
        }
    );
};

exports.sendResponse = function(request, response, next) {
    response.status(201).json({data: request._subscriptionSchema._doc});
    next();
};