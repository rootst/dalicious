'use strict';
var _ = require('lodash');

var config = require('../../../configs/config');
var Subscription = require(config.root + '/models/subscription');


exports.getPaginatedItems = function(request, response, next) {
    var copiedQueryStatement = _.clone(request.query);
    var page = request.query.page;

    copiedQueryStatement = _.omit(copiedQueryStatement, 'page');

    if ('status' in copiedQueryStatement) {
        if (copiedQueryStatement.status === '-PROCESSING') {
            delete copiedQueryStatement.status;
            copiedQueryStatement.status = {
                $in: _.difference(_.values(config.subscriptionStatus), [config.subscriptionStatus.processing])
            }
        }
    }

    if (_.isUndefined(page)) {
        page = 1;
    }

    var queryOptions = {};
    queryOptions.page = page;
    queryOptions.limit = config.pagination.limit;
    queryOptions.sort = {
        createdDate: -1
    }
    queryOptions.populate = [
        {path: 'owner'}
    ];
    
    Subscription.paginate(copiedQueryStatement, queryOptions, (errorResult, result) => {
        if (errorResult) {
            errorResult._status = 500;
            return next(errorResult);
        }
        if (!result) {
            var errorResult = {};
            errorResult._status = 404;
            return next(errorResult);
        }

        var hasMore = true;
        if (result.pages <= result.page) {
            hasMore = false;
        }

        var data = {
            total: result.total,
            hasMore: hasMore,
            items: result.docs,
            page: result.page
        };

        request._subscriptions = data;
        next();
    });
};

exports.postProcess = function(request, response, next) {
    _.forEach(request._subscriptions, (review) => {
        //
    });
    next();
};

exports.sendResponse = function(request, response, next) {
    response.status(200).json({data: request._subscriptions});
    next();
};