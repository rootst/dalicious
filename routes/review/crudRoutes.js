'use strict';
var express = require('express');
var passport = require('passport');
var _ = require('lodash');

var config = require('../../configs/config');
var userJudge = require(config.root + '/utilities/userJudge');
var List = require(config.root + '/routes/review/controllers/list');
var Create = require(config.root + '/routes/review/controllers/create');
var Retrieve = require(config.root + '/routes/review/controllers/retrieve');
var Update = require(config.root + '/routes/review/controllers/update');
var Delete = require(config.root + '/routes/review/controllers/delete');


var router = express.Router();

router.get('/',
    passport.authenticate('jwt', {session: false}),
    List.getPaginatedItems,
    List.postProcess,
    List.sendResponse
);
router.post('/',
    passport.authenticate('jwt', {session: false}),
    userJudge.requireRole(_.concat(config.administrators, config.consumers)),
    Create.createItem,
    Create.sendResponse
);
router.get('/:pk',
    passport.authenticate('jwt', {session: false}),
    userJudge.requireRole(_.concat(config.administrators, config.suppliers, config.consumers)),
    Retrieve.findItem,
    Retrieve.postProcess,
    Retrieve.sendResponse
);
router.put('/:pk',
    passport.authenticate('jwt', {session: false}),
    userJudge.requireRole(_.concat(config.administrators, config.suppliers, config.consumers)),
    Update.updateItem,
    Update.sendResponse
);
router.patch('/:pk',
    passport.authenticate('jwt', {session: false}),
    userJudge.requireRole(_.concat(config.administrators, config.suppliers, config.consumers)),
    Update.updateItem,
    Update.sendResponse
);
router.delete('/:pk',
    passport.authenticate('jwt', {session: false}),
    userJudge.requireRole(_.concat(config.administrators, config.consumers)),
    Delete.deleteItem,
    Delete.sendResponse
);

module.exports = router;
