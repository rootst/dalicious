'use strict';
var express = require('express');
var passport = require('passport');

var config = require('../../configs/config');
var userJudge = require(config.root + '/utilities/userJudge');


var router = express.Router();

module.exports = router;
