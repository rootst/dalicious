'use strict';
var _ = require('lodash');

var config = require('../../../configs/config');
var Review = require(config.root + '/models/review');
var Auction = require(config.root + '/models/auction');


exports.createItem = function(request, response, next) {
    var fields = request.body;
    var user = request.user;
    var auctionPk = fields.auction;

    Auction.findById(auctionPk, (foundError, foundAuction) => {
        if (foundError) {
            foundError._status = 500;
            return next(foundError);
        }
        if (!foundAuction) {
            var errorResult = {};
            errorResult._status = 404;
            return next(errorResult);
        }

        var processedFields = {};
        processedFields.owner = user;
        processedFields.auction = foundAuction;
        processedFields.type = fields.type;
        processedFields.content = fields.content;
        processedFields.pictures = fields.pictures;
        var review = new Review(processedFields);
        review.save((errorResult, result) => {
            if (errorResult) {
                errorResult._status = 500;
                return next(errorResult);
            }
            if (!result) {
                var errorResult = {};
                errorResult._status = 500;
                return next(errorResult);
            }

            foundAuction.status = config.auctionStatus.reviewCompleted;
            foundAuction.save((saveError, saved) => {
                if (saveError) {
                    saveError._status = 500;
                    return next(saveError);
                }

                request._reviewSchema = result;
                next();
            });
        });
    });
};

exports.sendResponse = function(request, response, next) {
    response.status(201).json({data: request._reviewSchema._doc});
    next();
};