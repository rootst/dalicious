'use strict';
var _ = require('lodash');
var express = require('express');
var passport = require('passport');
var jwt = require('jsonwebtoken');

var config = require('../configs/config');
var Login = require(config.root + '/routes/controllers/login');
var ImageUpload = require(config.root + '/routes/controllers/imageUpload');
var ReviewImage = require(config.root + '/routes/controllers/reviewImage');


var router = express.Router();

router.get('/', (request, response) => {response.render('index', {index: 'on'});});

router.post('/login',
    Login.authenticate,
    Login.generateToken,
    Login.sendResponse
);

router.post('/avatar',
    ImageUpload.imageUpload,
    ImageUpload.sendResponse
);
router.post('/reviewImage',
    ReviewImage.imageUpload,
    ReviewImage.sendResponse
);

router.get('/auth/facebook', 
    passport.authenticate('facebook', {
        scope: ['email'],
    }), (request, response, next) => {
        
    }
);
router.get('/auth/facebook/callback',
    passport.authenticate('facebook', {
        successRedirect: '/login_success',
        failureRedirect: '/login_fail',
        failureFlash: true,
        successFlash: true
    })
);
router.get('/login_success', (request, response, next) => {
    var token = jwt.sign(request.user._doc, config.sessionSecretKey, { expiresIn: '1d' });
    response.render('login_success', {jwt: token});
});
router.get('/login_fail', (req, res, next) => {
    res.render('socialAuthError', {error: req.flash('error')[0]});
});

router.get('/auth/kakao', 
    passport.authenticate('kakao', {

    }), (request, response, next) => {
        
    }
);
router.get('/auth/kakao/callback',
    passport.authenticate('kakao', {
        successRedirect: '/login_success',
        failureRedirect: '/login_fail',
        failureFlash: true,
        successFlash: true
    }), (request, response, next) => {
        
    }
);

router.get('/logout', (request, response) => {
    request.logout();
    response.redirect('/');
});

router.use('/users', require(config.root + '/routes/user/routes'));
router.use('/api/users', require(config.root + '/routes/user/crudRoutes'));
router.use('/auctions', require(config.root + '/routes/auction/routes'));
router.use('/api/auctions', require(config.root + '/routes/auction/crudRoutes'));
router.use('/bids', require(config.root + '/routes/bid/routes'));
router.use('/api/bids', require(config.root + '/routes/bid/crudRoutes'));
router.use('/subscriptions', require(config.root + '/routes/subscription/routes'));
router.use('/api/subscriptions', require(config.root + '/routes/subscription/crudRoutes'));
router.use('/reviews', require(config.root + '/routes/review/routes'));
router.use('/api/reviews', require(config.root + '/routes/review/crudRoutes'));
router.use('/feedbacks', require(config.root + '/routes/feedback/routes'));
router.use('/api/feedbacks', require(config.root + '/routes/feedback/crudRoutes'));

router.get('/biding_list', (request, response) => {
    response.render('biding_list');
});

//이용약관
router.get('/agreement', (request, response) => {response.render('agreement/agreement');});
//개인정보보호정책
router.get('/policy', (request, response) => {response.render('agreement/policy');});
router.get('/review/write/:pk', (request, response) => {
    var reviewId = request.params.pk;
    response.render('review-write', {reviewId: reviewId});
});

router.get('/supplier', (request, response) => {response.render('supplier', {supplier: 'on'});});
router.get('/review', (request, response) => {response.render('review', {review: 'on'});});
router.get('/introduce', (request, response) => {response.render('introduce', {introduce: 'on'});});
router.get('/subscription-service', (request, response) => {response.render('introduce-sub', {gnbStyle: 'white'});});
router.get('/partner', (request, response) => {response.render('introduce-partner', {gnbStyle: 'white', partner: 'on'});});
router.get('/ondemand-service', (request, response) => {response.render('introduce-on', {introduce: 'on'});});

router.get('/success', (request, response) => {response.render('request_success');});
router.get('/mypage', (request, response) => {
    var menu = request.query.menu;
    var activeClass = 'mypage-submenu__menus--in';
    if(menu === "default" || typeof menu === 'undefined') {
        if(request.isAuthenticated()) {
            var gender = request.user.gender;
            return response.render('mypage_default', {mypage_default: activeClass, gender: gender});
        }
        return response.redirect('/');
    } else if(menu === "request") {
        response.render('mypage_request', {mypage_request: activeClass});
    } else if(menu === "auth") {
        response.render('mypage_auth', {mypage_auth: activeClass});
    } else if(menu === "members") {
        response.render('mypage_members', {mypage_members: activeClass});
    } else if(menu === "bid") {
        response.render('mypage_bid', {mypage_bid: activeClass});
    } else if(menu === "request_list") {
        response.render('mypage_request_list', {mypage_request_list: activeClass});
    } else if(menu === "members_profile") {
        if(request.isAuthenticated()) {
            var availableZone = ['서울시', '경기도', '강원도', '충청남도', '충청북도', '경상남도', '경상북도', '전라남도', '전라북도', '제주시'];
            var availableCuisines = ['korean', 'chinese', 'japanese', 'western'];
 
            var myZone = request.user.address;
            var myCuisines = request.user.supplier.cuisines;

            availableCuisines = _.difference(availableCuisines, myCuisines);
            availableZone = _.difference(availableZone, myZone);

            response.render('mypage_members_profile', {
                mypage_members_profile: activeClass,
                availableZone: availableZone,
                availableCuisines: availableCuisines,
                myZone: myZone,
                myCuisines: myCuisines
            });
        } else {
            response.redirect('/');
        }
    } else if(menu === "members_auth") {
        response.render('mypage_members_auth', {mypage_members_auth: activeClass});
    } else if(menu === "admin_auction") {
        response.render('mypage_admin_auction', {mypage_admin_auction: activeClass});
    } else if(menu === "admin_auction_end") {
        response.render('mypage_admin_auction_end', {mypage_admin_auction_end: activeClass});
    } else if(menu === "admin_supplier_auth") {
        response.render('mypage_admin_supplier_auth', {mypage_admin_supplier_auth: activeClass});
    } else if(menu === "admin_supplier") {
        response.render('mypage_admin_supplier', {mypage_admin_supplier: activeClass});
    } else if(menu === "admin_consumer") {
        response.render('mypage_admin_consumer', {mypage_admin_consumer: activeClass});
    } else if(menu === "admin_subscription") {
        response.render('mypage_admin_subscription', {mypage_admin_subscription: activeClass});
    } else if(menu === "admin_feedback") {
        response.render('mypage_admin_feedback', {mypage_admin_feedback: activeClass});
    }
});

module.exports = router;
