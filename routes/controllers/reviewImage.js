'use strict';
var _ = require('lodash');

var config = require('../../configs/config');
var Aws = require(config.root + '/utilities/aws');
var Auction = require(config.root + '/models/auction');


exports.imageUpload = function(request, response, next) {
    var fields = request.body;
    var user = request.user;
    var auctionPk = fields.auction;

    Auction.findById(auctionPk, (foundError, foundAuction) => {
        if (foundError) {
            foundError._status = 500;
            return next(foundError);
        }
        if (!foundAuction) {
            var errorResult = {};
            errorResult._status = 404;
            return next(errorResult);
        }

        Aws.imageUpload('dalicious', 'auction/' + foundAuction._id + "/" + _.random(100000, 999999), 'public-read', request.files[0], (uploadError, uploaded) => {
            if (uploadError) {
                return next(uploadError);
            }
            request._imageLocation = uploaded.Location;
            next();
        });
    });
};

exports.sendResponse = function(request, response, next) {
    return response.status(200).json({data: request._imageLocation});
};