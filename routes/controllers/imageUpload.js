'use strict';
var _ = require('lodash');

var config = require('../../configs/config');
var User = require(config.root + '/models/user');
var Aws = require(config.root + '/utilities/aws');


exports.imageUpload = function(request, response, next) {
    var user = request.user;
    
    Aws.imageUpload('dalicious', 'user/' + user._id, 'public-read', request.files[0], (uploadError, uploaded) => {
        if (uploadError) {
            return next(uploadError);
        }

        user.avatar = uploaded.Location;
        user.save((saveErrorResult, saveResult) => {
            if (saveErrorResult) {
                saveErrorResult._status = 500;
                return next(saveErrorResult);
            }
            if (!saveResult) {
                var saveErrorResult = {};
                saveErrorResult._status = 500;
                return next(saveErrorResult);
            }

            request.user = saveResult;
            next();
        });
    });
};

exports.sendResponse = function(request, response, next) {
    response.status(200).json({data: request.user.avatar});
    next();
};