'use strict';
var _ = require('lodash');
var jwt = require('jsonwebtoken');

var config = require('./../../configs/config');
var User = require(config.root + '/models/user');


exports.authenticate = function(request, response, next) {
    let fields = request.body;

    var email = fields.email;
    var password = fields.password;

    User.findOne({'email': email}, (errorResult, result) => {
        if (errorResult) {
            errorResult._status = 500;
            return next(errorResult);
        }
        if (!result) {
            var errorResult = {};
            errorResult._status = 404;
            return next(errorResult);
        }

        result.comparePassword(password, (errorResult, isMatched) => {
            if (errorResult) {
                errorResult._status = 500;
                return next(errorResult);
            }
            if (!isMatched) {
                var passwordMatchFailed = {};
                passwordMatchFailed._status = 401;
                return next(passwordMatchFailed);
            }

            request.login(result._doc, (errorResult) => {
                if (errorResult) {
                    errorResult._status = 401;
                    return next(errorResult);
                }

                result.lastLoggedInDate = new Date();
                result.save((updateErrorResult, updateResult) => {
                    if (updateErrorResult) {
                        updateErrorResult._status = 500;
                        return next(updateErrorResult);
                    }

                    request._userSchema = updateResult;
                    next();
                });
            });
        });
    });
};

exports.generateToken = function(request, response, next) {
    request._userSchema = _.omit(request._userSchema, 'password');

    var token = jwt.sign(request._userSchema._doc, config.sessionSecretKey, { expiresIn: '1d' });
    request._userToken = token;
    next();
};

exports.sendResponse = function(request, response, next) {
    response.status(200).json({data: request._userToken});
    next();
};