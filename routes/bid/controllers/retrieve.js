'use strict';
var config = require('../../../configs/config');
var stringContains = require(config.root + '/utilities/stringContains');
var Bid = require(config.root + '/models/bid');


exports.findItem = function(request, response, next) {
    var pk = request.params.pk;
    var user = request.user;

    Bid.findById(pk, (errorResult, result) => {
        if (errorResult) {
            errorResult._status = 500;
            return next(errorResult);
        }
        if (!result) {
            var errorResult = {};
            errorResult._status = 404;
            return next(errorResult);
        }
        // check authenticate
        if ((String(result.owner) !== String(user._id)) && (!stringContains(user.role, config.administrators))) {
            var errorResult = {};
            errorResult._status = 403;
            return next(errorResult);
        }

        request._bidSchema = result;
        next();
    });
};

exports.postProcess = function(request, response, next) {
    next();
};

exports.sendResponse = function(request, response, next) {
    response.status(200).json({data: request._bidSchema._doc});
    next();
};