'use strict';
var _ = require('lodash');

var config = require('../../../configs/config');
var Bid = require(config.root + '/models/bid');
var User = require(config.root + '/models/user');
var Auction = require(config.root + '/models/auction');


exports.createItem = function(request, response, next) {
    var fields = request.body;
    var user = request.user;

    // NOTE: 떔빵, 어드민이 강제로 bidUser를 삽입시, id에서 email로 변경했기 때문에 이렇게 됫습니다.
    // 귀찮아서 한것이니 나중에 수정바람.
    if (typeof fields.bidUser !== 'undefined') {
        User.findOne({email: fields.bidUser})
        .then((foundUser) => {
            if (!foundUser) {
                var err = new Error('유저가 존재하지 않습니다.');
                err._status = 404;
                next(err);
                return;
            }
            var processedFields = {};
            // 여기수정부분
            processedFields.owner = foundUser;
            processedFields.auction = fields.auction;
            var price = _.replace(fields.price, new RegExp(",","g"), "");
            processedFields.price = _.toNumber(price);
            processedFields.proposal = fields.proposal;
            var bid = new Bid(processedFields);
            bid.save((errorResult, result) => {
                if (errorResult) {
                    errorResult._status = 500;
                    return next(errorResult);
                }
                if (!result) {
                    var errorResult = {};
                    errorResult._status = 500;
                    return next(errorResult);
                }

                request._bidSchema = result;
                next();
            });
        });

    } else {
        var processedFields = {};
        processedFields.owner = user;
        processedFields.auction = fields.auction;
        var price = _.replace(fields.price, new RegExp(",","g"), "");
        processedFields.price = _.toNumber(price);
        processedFields.proposal = fields.proposal;
        var bid = new Bid(processedFields);
        bid.save((errorResult, result) => {
            if (errorResult) {
                errorResult._status = 500;
                return next(errorResult);
            }
            if (!result) {
                var errorResult = {};
                errorResult._status = 500;
                return next(errorResult);
            }

            request._bidSchema = result;
            next();
        });
    }
};

exports.sendResponse = function(request, response, next) {
    response.status(201).json({data: request._bidSchema._doc});
    next();
};