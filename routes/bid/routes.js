'use strict';
var express = require('express');
var passport = require('passport');
var _ = require('lodash');

var config = require('../../configs/config');
var userJudge = require(config.root + '/utilities/userJudge');

var User = require(config.root + '/models/user');
var Auction = require(config.root + '/models/auction');


var router = express.Router();

router.get('/',
    (request, response) => {
        var auctionId = request.query.auction;
        Auction
        .findOne({_id: auctionId})
        .populate({path: 'chosenBids', populate: { path: 'owner', model: 'User'}})
        .exec(function(err, auction){
            console.log(auction);
            if(auction) {
                response.render('biding_list', {auction: auction});
            } else {
                //WTF
            }
        });
    }
);

module.exports = router;
