'use strict';
var config = require('../../../configs/config');
var stringContains = require(config.root + '/utilities/stringContains');
var Feedback = require(config.root + '/models/feedback');


exports.findItem = function(request, response, next) {
    var pk = request.params.pk;

    Feedback.findById(pk, (errorResult, result) => {
        if (errorResult) {
            errorResult._status = 500;
            return next(errorResult);
        }
        if (!result) {
            var errorResult = {};
            errorResult._status = 404;
            return next(errorResult);
        }
        // check authenticate
        if ((String(result.owner) !== String(user._id)) && (!stringContains(user.role, config.administrators))) {
            var errorResult = {};
            errorResult._status = 403;
            return next(errorResult);
        }

        request._feedbackSchema = result;
        next();
    });
};

exports.postProcess = function(request, response, next) {
    next();
};

exports.sendResponse = function(request, response, next) {
    response.status(200).json({data: request._feedbackSchema._doc});
    next();
};