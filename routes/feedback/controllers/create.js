'use strict';
var _ = require('lodash');

var config = require('../../../configs/config');
var Feedback = require(config.root + '/models/feedback');


exports.createItem = function(request, response, next) {
    var fields = request.body;
    var user = request.user;

    var processedFields = {};
    processedFields.name = fields.name;
    processedFields.phone = fields.phone;
    processedFields.content = fields.content;
    var feedback = new Feedback(processedFields);
    feedback.save((errorResult, result) => {
        if (errorResult) {
            errorResult._status = 500;
            return next(errorResult);
        }
        if (!result) {
            var errorResult = {};
            errorResult._status = 500;
            return next(errorResult);
        }

        request._feedbackSchema = result;
        next();
    });
};

exports.sendResponse = function(request, response, next) {
    response.status(201).json({data: request._feedbackSchema._doc});
    next();
};