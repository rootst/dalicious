'use strict';
var express = require('express');
var _ = require('lodash');

var config = require('../../configs/config');
var List = require(config.root + '/routes/feedback/controllers/list');
var Create = require(config.root + '/routes/feedback/controllers/create');
var Retrieve = require(config.root + '/routes/feedback/controllers/retrieve');
var Update = require(config.root + '/routes/feedback/controllers/update');
var Delete = require(config.root + '/routes/feedback/controllers/delete');


var router = express.Router();

router.get('/',
  List.getPaginatedItems,
  List.postProcess,
  List.sendResponse
);
router.post('/',
  Create.createItem,
  Create.sendResponse
);
router.get('/:pk',
  Retrieve.findItem,
  Retrieve.postProcess,
  Retrieve.sendResponse
);
router.put('/:pk',
  Update.updateItem,
  Update.sendResponse
);
router.patch('/:pk',
  Update.updateItem,
  Update.sendResponse
);
router.delete('/:pk',
  Delete.deleteItem,
  Delete.sendResponse
);


module.exports = router;
