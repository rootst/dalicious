'use strict';
var express = require('express');
var passport = require('passport');
var _ = require('lodash');

var config = require('../../configs/config');
var userJudge = require(config.root + '/utilities/userJudge');
var userChecker = require(config.root + '/utilities/userChecker');
var List = require(config.root + '/routes/auction/controllers/list');
var Create = require(config.root + '/routes/auction/controllers/create');
var Retrieve = require(config.root + '/routes/auction/controllers/retrieve');
var Update = require(config.root + '/routes/auction/controllers/update');
var Delete = require(config.root + '/routes/auction/controllers/delete');


var router = express.Router();

router.get('/',
    passport.authenticate('jwt', {session: false}),
    List.getPaginatedItems,
    List.postProcess,
    List.sendResponse
);
router.post('/',
    passport.authenticate('jwt', {session: false}),
    userJudge.requireRole(_.concat(config.administrators, config.consumers)),
    userChecker.verifyMobile,
    (request, response, next) => {
      var address = _.split(request.body.serviceAddress, ' ')[0];
      if (!(_.includes(address, '서울') ||
      _.includes(address, '인천') ||
      _.includes(address, '대구') ||
      _.includes(address, '광주') ||
      _.includes(address, '부산') ||
      _.includes(address, '울산') ||
      _.includes(address, '대전') ||
      _.includes(address, '테스트') ||
      _.includes(address, '경상남도') ||
      _.includes(address, '경기도') ||
      _.includes(address, '강원도') ||
      _.includes(address, '충청남도') ||
      _.includes(address, '충청북도') ||
      _.includes(address, '전라남도') ||
      _.includes(address, '전라북도') ||
      _.includes(address, '경상남도') ||
      _.includes(address, '경상북도'))) {
        let errorMessage = {
          _status: 500,
          _message: '주소가 옳바르지 않습니다. 아래와 같은 형식으로 작성해주세요. ex) 서울시 A구(군) B로(B동), 경상남도 울진군 ...'
        };
        return next(errorMessage);
      }
      next();
    },
    Create.createItem,
    Create.generateSMS,
    Create.sendResponse
);
router.get('/:pk',
    passport.authenticate('jwt', {session: false}),
    userJudge.requireRole(_.concat(config.administrators, config.suppliers, config.consumers)),
    Retrieve.findItem,
    Retrieve.postProcess,
    Retrieve.sendResponse
);
router.put('/:pk',
    passport.authenticate('jwt', {session: false}),
    userJudge.requireRole(_.concat(config.administrators, config.suppliers, config.consumers)),
    Update.updateItem,
    Update.sendResponse
);
router.patch('/:pk',
    passport.authenticate('jwt', {session: false}),
    userJudge.requireRole(_.concat(config.administrators, config.suppliers, config.consumers)),
    Update.updateItem,
    Update.sendResponse
);
router.delete('/:pk',
    passport.authenticate('jwt', {session: false}),
    userJudge.requireRole(_.concat(config.administrators, config.consumers)),
    Delete.deleteItem,
    Delete.sendResponse
);

module.exports = router;
