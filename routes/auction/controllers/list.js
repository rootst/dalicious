'use strict';
var _ = require('lodash');
var mongoose = require('mongoose');

var config = require('../../../configs/config');
var Auction = require(config.root + '/models/auction');


exports.getPaginatedItems = function(request, response, next) {
    var copiedQueryStatement = _.clone(request.query);
    var page = request.query.page;
    
    copiedQueryStatement = _.omit(copiedQueryStatement, 'page');

    if ('targetSuppliers' in copiedQueryStatement) {
        var targetSupplier = copiedQueryStatement.targetSuppliers;
        copiedQueryStatement.targetSuppliers = {
            $elemMatch: {user: {$in: [new mongoose.Types.ObjectId(targetSupplier)]}}
        }
    }

    if ('status' in copiedQueryStatement) {
        if (copiedQueryStatement.status === '-PROCESSING') {
            delete copiedQueryStatement.status;
            copiedQueryStatement.status = {
                $in: _.difference(_.values(config.auctionStatus), [config.auctionStatus.processing])
            }
        }
    }

    if (_.isUndefined(page)) {
        page = 1;
    }

    var queryOptions = {};
    queryOptions.page = page;
    queryOptions.limit = config.pagination.limit;
    queryOptions.sort = {
        createdDate: -1
    }
    queryOptions.populate = [
        {
            path: 'chosenBids',
            populate: {
                path: 'owner',
                model: 'User'
            },
        }, {
            path: 'winner',
            populate: {
                path: 'owner',
                model: 'User'
            },
        },
        {
            path: 'owner',
            model: 'User',
            select: 'displayName phone.number'
        },
    ];
    
    Auction.paginate(copiedQueryStatement, queryOptions, (errorResult, result) => {
        if (errorResult) {
            errorResult._status = 500;
            return next(errorResult);
        }
        if (!result) {
            var errorResult = {};
            errorResult._status = 404;
            return next(errorResult);
        }

        var hasMore = true;
        if (result.pages <= result.page) {
            hasMore = false;
        }

        var data = {
            total: result.total,
            hasMore: hasMore,
            items: result.docs,
            page: result.page
        };

        request._auctions = data;
        next();
    });
};

exports.postProcess = function(request, response, next) {
    _.forEach(request._auctions, (bid) => {
        //
    });
    next();
};

exports.sendResponse = function(request, response, next) {
    response.status(200).json({data: request._auctions});
    next();
};