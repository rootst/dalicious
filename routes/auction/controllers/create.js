'use strict';
var _ = require('lodash');

var config = require('../../../configs/config');
var Auction = require(config.root + '/models/auction');
var Bid = require(config.root + '/models/bid');
var User = require(config.root + '/models/user');
var Slack = require(config.root + '/utilities/slack');
var Aws = require(config.root + '/utilities/aws');


exports.createItem = function(request, response, next) {
    var fields = request.body;
    var user = request.user;

    var processedFields = {};
    processedFields.owner = user;
    processedFields.peopleCounts = fields.peopleCounts;
    var budget = _.replace(fields.budget, new RegExp(",","g"), "");
    processedFields.budget = _.toNumber(budget);
    processedFields.serviceDate = fields.serviceDate;
    var currentDateTime = new Date(fields.serviceDate);
    var startDateTime = currentDateTime.setHours(fields.serviceStartTime);
    processedFields.serviceStartTime = startDateTime;
    processedFields.serviceAddress = fields.serviceAddress;
    processedFields.cuisines = fields.cuisines;
    processedFields.description = fields.description;
    processedFields.status = config.auctionStatus.processing;
    var auction = new Auction(processedFields);
    auction.save((errorResult, result) => {
        if (errorResult) {
            errorResult._status = 500;
            return next(errorResult);
        }
        if (!result) {
            var errorResult = {};
            errorResult._status = 500;
            return next(errorResult);
        }

        request._auctionSchema = result;
        next();
    });
};


exports.generateSMS = function(request, response, next) {
    var phoneNumber = request.user.phone.number;
    var messageContent = '';

    messageContent = 'Dalicious입니다. 귀하의 경매가 시작되었습니다. 앞으로 24시간 후 경매가 종료됩니다.';

    Aws.publishSMS(
        messageContent,
        'string',
        phoneNumber,
        (sendError, sent) => {
            if (sendError) {
                return next(sendError);
            }
            Slack.sendMessage('공급자가 회원가입하였습니다.');
            next();
        }
    );
};

exports.sendResponse = function(request, response, next) {
    response.status(201).json({data: request._auctionSchema._doc});
    next();
};
