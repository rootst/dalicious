'use strict';
var _ = require('lodash');
var schedule = require('node-schedule');

var config = require('../../../configs/config');
var stringContains = require(config.root + '/utilities/stringContains');
var Aws = require(config.root + '/utilities/aws');
var Auction = require(config.root + '/models/auction');
var Bid = require(config.root + '/models/bid');


exports.updateItem = function(request, response, next) {
    var pk = request.params.pk;
    var user = request.user;
    var fields = request.body;
    var populateOptions = [{path: 'owner', model: 'User'}];

    Auction.findById(pk).populate(populateOptions).exec((errorResult, result) => {
        if (errorResult) {
            errorResult._status = 500;
            return next(errorResult);
        }
        if (!result) {
            var errorResult = {};
            errorResult._status = 404;
            return next(errorResult);
        }
        // check authenticate
        if ((String(result.owner._id) !== String(user._id)) && (!stringContains(user.role, config.administrators))) {
            var errorResult = {};
            errorResult._status = 403;
            return next(errorResult);
        }

        _.forOwn(fields, (value, key) => {
            if (key === 'winner') {
                result.status = config.auctionStatus.success;

                _.forEach(result.chosenBids, (chosenBid) => {
                    // FIXME : DO NOT ASYNC
                    Bid.findById(chosenBid).populate('owner').exec((bidFoundError, foundBid) => {
                        if (bidFoundError) {
                            bidFoundError._status = 500;
                            return next(bidFoundError);
                        }
                        if (!foundBid) {
                            var errorResult = {};
                            errorResult._status = 404;
                            return next(errorResult);
                        }

                        var phoneNumber = foundBid.owner.phone.number;
                        var messageContent = '';
                        // 투찰에 낙찰한 사람에게만 보낸다.
                        if (String(foundBid._id) === String(value)) {
                            messageContent = 'Dalicious입니다. 귀하의 투찰이 낙찰되었습니다.';
                            Aws.publishSMS(
                                messageContent,
                                'string',
                                phoneNumber,
                                (sendError, sent) => {
                                    if (sendError) {
                                        return next(sendError);
                                    }
                                }
                            );
                        }
                    });
                });

                // 유찰 전용
                Bid.find({auction: result._id}).populate('owner').then((bids) => {
                    if (bidFoundError) {
                        bidFoundError._status = 500;
                        return next(bidFoundError);
                    }

                    _.forOwn(bids, (bid) => {
                        var phoneNumber = bid.owner.phone.number;
                        var messageContent = '';
                        messageContent = 'Dalicious입니다. 귀하의 투찰이 유찰되었습니다. 다음에 다시 시도해주시기 바랍니다.';
                        Aws.publishSMS(
                            messageContent,
                            'string',
                            phoneNumber,
                            (sendError, sent) => {
                                if (sendError) {
                                    return next(sendError);
                                }
                            }
                        );
                    });
                });

                //#FIXME: 디버그용 1분으로 해놨슴니당
                var triggerDate = new Date();
                triggerDate.setMinutes(triggerDate.getMinutes() + 1);

                // set status to service end.
                var job = schedule.scheduleJob(result.serviceStartTime, function(auctionSchema) {
                //var job = schedule.scheduleJob(triggerDate, function(auctionSchema) {
                    auctionSchema.status = config.auctionStatus.serviceEnd;
                    auctionSchema.save((saveError, saved) => {
                        if (saveError) {
                            console.error(saveError);
                            return;
                        }

                        var phoneNumber = saved.owner.phone.number;
                        var messageContent = '';
                        messageContent = 'Dalicious입니다. 서비스가 시작되셨나요? 서비스 후 후기를 작성해주세요.';
                        Aws.publishSMS(
                            messageContent,
                            'string',
                            phoneNumber,
                            (sendError, sent) => {
                                if (sendError) {
                                    console.error(sendError);
                                    return;
                                }

                            }
                        );
                    });
                }.bind(null, result));
            }

            if (_.isArray(result[key])) {
                if (key === 'targetSuppliers') {
                    var dictValue = {};
                    dictValue.bidable = true;
                    dictValue.user = value;
                    result[key].push(dictValue);
                } else {
                    result[key].push(value);
                }
            } else {
                result[key] = value;
            }
        });
        result.save((updateErrorResult, updateResult) => {
            if (updateErrorResult) {
                updateErrorResult._status = 500;
                return next(updateErrorResult);
            }
            if (!updateResult) {
                var updateErrorResult = {};
                updateErrorResult._status = 500;
                return next(updateErrorResult);
            }

            request._auctionSchema = updateResult;
            next();
        });
    });
};

exports.sendResponse = function(request, response, next) {
    response.status(200).json({data: request._auctionSchema._doc});
    next();
};
