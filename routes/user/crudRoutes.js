'use strict';
var express = require('express');
var passport = require('passport');
var _ = require('lodash');

var config = require('../../configs/config');
var userJudge = require(config.root + '/utilities/userJudge');
//var existCheck = require(config.root + '/routes/user/controllers/existCheck');
var List = require(config.root + '/routes/user/controllers/list');
var Create = require(config.root + '/routes/user/controllers/create');
var Retrieve = require(config.root + '/routes/user/controllers/retrieve');
var Update = require(config.root + '/routes/user/controllers/update');
var Delete = require(config.root + '/routes/user/controllers/delete');
var PhoneVerified = require(config.root + '/routes/user/controllers/phoneVerified');


var router = express.Router();

router.get('/',
    List.getPaginatedItems,
    List.postProcess,
    List.sendResponse
);
router.post('/',
    Create.createItem,
    Create.authenticate,
    Create.generateToken,
    Create.celebrate,
    Create.sendResponse
);
router.get('/:pk',
    passport.authenticate('jwt', {session: false}),
    userJudge.requireRole(_.concat(config.administrators, config.consumers, config.suppliers, config.spaceSuppliers)),
    Retrieve.findItem,
    Retrieve.postProcess,
    Retrieve.sendResponse
);
router.put('/:pk',
    passport.authenticate('jwt', {session: false}),
    userJudge.requireRole(_.concat(config.administrators, config.consumers, config.suppliers, config.spaceSuppliers)),
    Update.updateItem,
    Update.sendResponse
);
router.patch('/:pk',
    passport.authenticate('jwt', {session: false}),
    userJudge.requireRole(_.concat(config.administrators, config.consumers, config.suppliers, config.spaceSuppliers)),
    Update.updateItem,
    Update.sendResponse
);
router.delete('/:pk',
    passport.authenticate('jwt', {session: false}),
    userJudge.requireRole(_.concat(config.administrators, config.consumers, config.suppliers, config.spaceSuppliers)),
    Delete.deleteItem,
    Delete.sendResponse
);

router.post('/:pk/phone_verified',
    passport.authenticate('jwt', {session: false}),
    userJudge.requireRole(_.concat(config.administrators, config.consumers, config.suppliers, config.spaceSuppliers)),
    PhoneVerified.process,
    PhoneVerified.generateSMS,
    PhoneVerified.sendResponse
);

module.exports = router;
