'use strict';
var _ = require('lodash');

var config = require('../../../configs/config');
var User = require(config.root + '/models/user');


exports.getPaginatedItems = function(request, response, next) {
    var copiedQueryStatement = _.clone(request.query);
    var page = request.query.page;

    copiedQueryStatement = _.omit(copiedQueryStatement, 'page');

    if ('authenticated' in copiedQueryStatement) {
        copiedQueryStatement["supplier.authenticated"] = (copiedQueryStatement.authenticated === 'true');
        delete copiedQueryStatement.authenticated;
        copiedQueryStatement.role = {
            $in: config.suppliers
        }
    }

    if ('consumerOnly' in copiedQueryStatement) {
        copiedQueryStatement.role = ['CONSUMER'];
        delete copiedQueryStatement.consumerOnly;
    }

    if (_.isUndefined(page)) {
        page = 1;
    }

    var queryOptions = {};
    queryOptions.page = page;
    queryOptions.limit = config.pagination.limit;
    queryOptions.sort = {
        joinedDate: -1
    }
    queryOptions.populate = [
        {path: 'owner'}
    ];
    
    User.paginate(copiedQueryStatement, queryOptions, (errorResult, result) => {
        if (errorResult) {
            errorResult._status = 500;
            return next(errorResult);
        }
        if (!result) {
            var errorResult = {};
            errorResult._status = 404;
            return next(errorResult);
        }

        var hasMore = true;
        if (result.pages <= result.page) {
            hasMore = false;
        }

        var data = {
            total: result.total,
            hasMore: hasMore,
            items: result.docs,
            page: result.page
        };

        request._users = data;
        next();
    });
};

exports.postProcess = function(request, response, next) {
    _.forEach(request._users, (user) => {
        user = _.omit(user, 'password');
    });
    next();
};

exports.sendResponse = function(request, response, next) {
    response.status(200).json({data: request._users});
    next();
};