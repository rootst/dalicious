'use strict';
var _ = require('lodash');
var jwt = require('jsonwebtoken');

var config = require('../../../configs/config');
var stringContains = require(config.root + '/utilities/stringContains');
var User = require(config.root + '/models/user');


exports.updateItem = function(request, response, next) {
    var pk = request.params.pk;
    var user = request.user;
    var fields = request.body;

    User.findById(pk, (errorResult, result) => {
        if (errorResult) {
            errorResult._status = 500;
            return next(errorResult);
        }
        if (!result) {
            var errorResult = {};
            errorResult._status = 404;
            return next(errorResult);
        }
        // check authenticate
        if ((String(result._id) !== String(user._id)) && (!stringContains(user.role, config.administrators))) {
            var errorResult = {};
            errorResult._status = 403;
            return next(errorResult);
        }

        // 유저의 쉐프 인증인 경우, 오기전에 핸드폰 체크하고 리젝한다.
        if (_.has(fields, 'role')) {
            if (_.indexOf(config.suppliers, fields.role) !== -1) {
                if (!result.phone.verified) {
                    var updateErrorResult = {};
                    updateErrorResult._status = 403;
                    return next(updateErrorResult);
                }
            }
        }

        _.forOwn(fields, (value, key) => {
            if (key === 'authenticated') {
                result.supplier.authenticated = value;
            }

            if (key === 'cuisines') {
                result['supplier'][key] = value;
            }

            if (key === 'menus') {
                result['supplier'][key] = value;
            }

            if (key === 'role') {
                if (_.indexOf(config.suppliers, value) !== -1) {
                    // supplier case
                    result.supplier = {};
                    result.supplier.authenticated = false;
                    result.supplier.cuisines = [];
                    result.supplier.menus = [];
                }
            }

            if (_.isArray(result[key])) {
                if (key === 'address') {
                    result[key] = value;
                } else {
                    result[key].push(value);
                }
            } else {
                result[key] = value;
            }
        });

        result.save((updateErrorResult, updateResult) => {
            if (updateErrorResult) {
                updateErrorResult._status = 500;
                return next(updateErrorResult);
            }
            if (!updateResult) {
                var updateErrorResult = {};
                updateErrorResult._status = 500;
                return next(updateErrorResult);
            }

            request._userSchema = updateResult;
            next();
        });
    });
};

exports.generateToken = function(request, response, next) {
    var copiedUser = _.clone(request._userSchema._doc);
    copiedUser = _.omit(copiedUser, 'password');

    var token = jwt.sign(copiedUser, config.sessionSecretKey, { expiresIn: '1d' });
    request._userToken = token;
    return resolve(token);
};

exports.sendResponse = function(request, response, next) {
    response.status(200).json({data: request._userToken});
    next();
};