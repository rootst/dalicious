'use strict';
var config = require('./../../../configs/config');
var User = require(config.root + '/models/user');


var email = function(queryStatement, callback) {
    delete queryStatement.page;
    delete queryStatement.limit;

    var query = {
        "email.address": queryStatement.q
    };

    User.count(query).then((result) => {
        callback(null, result);
        return null;

    }).catch((errorResult) => {
        if (errorResult) {
            errorResult._status = 500;
            callback(errorResult, null);
        }
    });
};

exports.email = function(request, response, next) {
    email(request.query, (errorResult, result) => {
        if (errorResult) {
            console.error(errorResult);
            return next(errorResult);
        }
        response.format({
            json: () => {
                // inspired by Stripe's API response for list objects
                response.json({
                    data: result
                });
            }
        });
        next();
    });
};
