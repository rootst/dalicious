'use strict';
var _ = require('lodash');
var jwt = require('jsonwebtoken');

var config = require('../../../configs/config');
var stringContains = require(config.root + '/utilities/stringContains');
var Aws = require(config.root + '/utilities/aws');
var Slack = require(config.root + '/utilities/slack');
var User = require(config.root + '/models/user');


exports.createItem = function(request, response, next) {
    var fields = request.body;
    var role;

    if (!stringContains(fields.role, config.consumers)) {
        role = _.concat(fields.role, 'CONSUMER');
    } else {
        role = fields.role;
    }
    var processedFields = {};
    processedFields.role = role;
    processedFields.email = fields.email;
    processedFields.password = fields.password;
    processedFields.displayName = fields.displayName;
    
    if (stringContains(fields.role, config.suppliers)) {
        processedFields.supplier = {
            authenticated: false,
            cuisines: [],
            menus: []
        };
    }
    var user = new User(processedFields);
    user.save((errorResult, result) => {
        if (errorResult) {
            errorResult._status = 500;
            return next(errorResult);
        }
        if (!result) {
            var errorResult = {};
            errorResult._status = 500;
            return next(errorResult);
        }

        request._userSchema = result;
        next();
    });
};

exports.authenticate = function(request, response, next) {
    request.login(request._userSchema, (errorResult) => {
        if (errorResult) {
            errorResult._status = 401;
            return next(errorResult);
        }

        next();
    });
};

exports.generateToken = function(request, response, next) {
    request._userSchema = _.omit(request._userSchema, 'password');

    var token = jwt.sign(request._userSchema._doc, config.sessionSecretKey, { expiresIn: '1d' });
    request._userToken = token;
    next();
};

exports.celebrate = function(request, response, next) {
    if (stringContains(request.user.role, config.suppliers)) {
        Slack.sendMessage('공급자가 회원가입하였습니다.');
        next();
    } else {
        next();
    }
};

exports.sendResponse = function(request, response, next) {
    response.status(201).json({data: request._userToken});
    next();
};