'use strict';
var _ = require('lodash');

var config = require('../../../configs/config');
var stringContains = require(config.root + '/utilities/stringContains');
var Aws = require(config.root + '/utilities/aws');
var User = require(config.root + '/models/user');


exports.process = function(request, response, next) {
    var pk = request.params.pk;
    var user = request.user;
    var fields = request.body;

    User.findById(pk, (errorResult, result) => {
        if (errorResult) {
            errorResult._status = 500;
            return next(errorResult);
        }
        if (!result) {
            var errorResult = {};
            errorResult._status = 404;
            return next(errorResult);
        }
        // check authenticate
        if ((String(result._id) !== String(user._id)) && (!stringContains(user.role, config.administrators))) {
            var errorResult = {};
            errorResult._status = 403;
            return next(errorResult);
        }
        /*
        // already verified
        if (result.phone.verified) {
            var errorResult = {};
            errorResult._status = 400;
            return next(errorResult);
        }
        */

        if (_.isUndefined(fields.phoneNumber)) {
            // 인증 번호 완료 부분
            if (result.phone.temporary !== _.toNumber(fields.authNumber)) {
                var errorResult = {};
                errorResult._status = 400;
                return next(errorResult);
            }

            result.phone.temporary = undefined;
            result.phone.verified = true;
            result.save((saveError, saved) => {
                if (saveError) {
                    saveError._status = 500;
                    return next(saveError);
                }
                if (!saved) {
                    var saveError = {};
                    saveError._status = 500;
                    return next(saveError);
                }

                request.user = saved;
                next();
            });

        } else {
            // 인증 번호 생성 부분
            var randomValue = _.random(100000, 999999, false);
            result.phone.temporary = randomValue;
            result.phone.verified = false;
            result.phone.number = fields.phoneNumber;
            result.save((saveError, saved) => {
                if (saveError) {
                    saveError._status = 500;
                    return next(saveError);
                }
                if (!saved) {
                    var saveError = {};
                    saveError._status = 500;
                    return next(saveError);
                }

                request._randomValue = randomValue;
                request.user = result;
                next();
            });
        }
    });
};

exports.generateSMS = function(request, response, next) {
    var phoneNumber = request.user.phone.number;
    var messageContent = '';
    var fields = request.body;

    if (_.isUndefined(fields.phoneNumber)) {
        // 인증 번호 완료 부분
        messageContent =
            'Dalicious입니다. 귀하의 핸드폰이 인증되었습니다.';
    } else {
        // 인증 번호 생성 부분
        messageContent =
            'Dalicious입니다. 귀하의 인증번호는 ' + request._randomValue + ' 입니다.';
    }

    Aws.publishSMS(
        messageContent,
        'string',
        phoneNumber,
        (sendError, sent) => {
            if (sendError) {
                return next(sendError);
            }
            console.log(sent);
            next();
        }
    );
};

exports.sendResponse = function(request, response, next) {
    response.status(200).json({});
    next();
};