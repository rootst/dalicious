'use strict';
var config = require('../../../configs/config');
var stringContains = require(config.root + '/utilities/stringContains');
var User = require(config.root + '/models/user');


exports.findItem = function(request, response, next) {
    var pk = request.params.pk;

    User.findById(pk, (errorResult, result) => {
        if (errorResult) {
            errorResult._status = 500;
            return next(errorResult);
        }
        if (!result) {
            var errorResult = {};
            errorResult._status = 404;
            return next(errorResult);
        }

        request._userSchema = result;
        next();
    });
};

exports.postProcess = function(request, response, next) {
    next();
};

exports.sendResponse = function(request, response, next) {
    response.status(200).json({data: request._userSchema._doc});
    next();
};