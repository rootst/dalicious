'use strict';
var express = require('express');
var passport = require('passport');

var config = require('../../configs/config');
var userJudge = require(config.root + '/utilities/userJudge');


var router = express.Router();

router.get('/:pk',
    passport.authenticate('jwt', {session: false}),
    ((request, response, next) => {
        response.render('myPage', {});
        next();
    })
);

module.exports = router;
