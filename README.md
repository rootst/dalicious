# README #

This README would normally document whatever steps are necessary to get your application up and running.

### 환경설정 ###

* ec2에 git과 pm2를 설치한다.
* 설치 후에 npm install 하고 sudo pm2 start pm2.yml로 실행한다. monit으로 확인하며 kill로 끈다.
* production은 redis를 세션공유로 사용하는데, redis의 경우 세팅하는 방법은 아래 기술한다.

### Elastic Cache ###

* node를 만든다.
* security group으로 redis쪽 sg의 inbound로 redis port(6379)를 설정하고 source를 ec2 sg로 한다.
* ec2 sg의 inbound에 redis쪽 sg로 설정한다.
* amazon linux에서 redis접근하기는 nc -v redis.mydomain.com 6379

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

### Globally install ###

* grunt-cli
* ruby
* sass