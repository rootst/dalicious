'use strict';
// NOTE: DO NOT USE UTILITIES
/* INITIALIZE BEGIN */

// set default node js environment to development
process.env.NODE_ENV = process.env.NODE_ENV || 'development';
if (process.env.NODE_ENV === 'development') {
    process.env.PORT = process.env.PORT || 3000;
} else if (process.env.NODE_ENV === 'production') {
    process.env.PORT = process.env.PORT || 8080;
} else {
    console.error('NODE_ENV ERROR');
    return;
}

/* INITIALIZE END */

var express = require('express');

var config = require('./configs/config');
var app = express();


// Initialize DB
require('./configs/database')(() => {
    // NOTE: DO NOT CHANGE THIS SEQUENCE.
    require(config.root + '/configs/express')(app);
    require(config.root + '/configs/passport')(app, require('passport'));
    require(config.root + '/configs/i18n')(app);

    app.use((request, response, next) => {
            response.locals = {
                user: request.user,
                authenticated: request.isAuthenticated()
            };
        next();
    });
    app.use('/', require('./routes/routes'));

    require(config.root + '/utilities/userJudge');
    require('./utilities/errorHandling')(app);
    require('./utilities/scheduleLoader').loadAuctionProcessing();
    require('./utilities/scheduleLoader').loadAuctionServiceEnd();

    app.listen(config.port, () => {
        console.log('running');
    });
});

// Expose app
exports = module.exports = app;
